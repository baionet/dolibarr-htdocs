<?php
/* Copyright (C) 2013, 2015-2021 Samuel Thibault <samuel.thibault@ens-lyon.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

require_once("./pre.inc.php");
require_once("./aquilenet.lib.php");
require_once(DOL_DOCUMENT_ROOT."/adherents/class/adherent.class.php");
require_once(DOL_DOCUMENT_ROOT."/adherents/class/adherent_type.class.php");

// Security check
$result=restrictedArea($user,'banque');

$langs->load("companies");
$langs->load("commercial");
$langs->load("bills");
$langs->load("banks");
$langs->load("users");

$rowid=isset($_GET["rowid"])?$_GET["rowid"]:$_POST["rowid"];

if ($rowid)
{
	$adherent = new Adherent($db);
	$adherent->fetch($rowid);

	$nom_complet = $adherent->$dolibarr_nom;
	if ($adherent->$dolibarr_prenom)
		$nom_complet = $adherent->$dolibarr_prenom." ".$nom_complet;

	$last = $adherent->datefin;
	$jour_last = date("d", $last);
	$mois_last = date("m", $last);
	$annee_last = date("Y", $last);

	$next = dol_time_plus_duree($last,1,'d');

	$montant = get_cotis($db, $adherent->id);

	if ($adherent->typeid == 1)
	{
		/* Membre adhérent, annuel */
		$next_end = dol_time_plus_duree($last,1,'y');
		$label = "$annee_last";
	}
	else if ($adherent->typeid == 2)
	{
		/* Membre adhérent préférentiel, annuel */
		$next_end = dol_time_plus_duree($last,1,'y');
		$label = "$annee_last";
	}
	else if ($adherent->typeid == 6)
	{
		/* Membre adhérent moral, annuel */
		$next_end = dol_time_plus_duree($last,1,'y');
		$label = "$annee_last";
	}
	else if ($adherent->typeid == 3)
	{
		/* Membre bienfaiteur, mensuel */
		$next_end = dol_time_plus_duree($last,1,'m');
		$label = "$mois_last $annee_last";
	}
	else
		$error++;

	$tiers = new Societe($db);
	$result = $tiers->fetch('', $nom_complet);

	$abos = somme_abos($db, $tiers);

	$bac = new CompanyBankAccount($db);
	$bac->fetch(0,$tiers->id);

}

llxHeader();

if ($_POST["action"] == "charge")
{
	$db->begin();

	if (!$rowid)
		$error++;

	$adherent->$dolibarr_cotisation($next, $montant, 0, '', '', '', '', '', $next_end);
	$account_adh = compte_adherent($db, $adherent);
	if (transfer($db, $account_adh->id, $COMPTE_PRODUIT_COTISATIONS, $next, "Cotisation $nom_complet ".$label, $montant, $user)) {
		print("\n\n erreur produit cotisation\n\n");
		$error++;
	}

	//$error++;

	if (! $error)
	{
		$db->commit();
		print "--- end ok, adherent $nom_complet, echeance ".dol_print_date($next,'day')." - ".dol_print_date($next_end,'day')." montant ".$montant;
	}
	else
	{
		print "--- end error $error, adherent $nom_complet, echeance ".dol_print_date($next,'day')." - ".dol_print_date($next_end,'day')." montant ".$montant;
		$db->rollback();
	}

}

if ($_POST["action"] == "rappel")
{
	$montant = preg_replace('/(\...).*/', '\1', $montant);
	$msg_abos = "";
	if ($abos)
		$msg_abos = " et ".$abos."€ de services";
	$adherent->send_an_email("Bonjour,

L'anniversaire de votre cotisation est arrivé. Merci pour votre
soutien, ce serait chouette que cela continue :) Pourriez-vous nous
envoyer le règlement de la cotisation pour $annee_last ($montant €
de cotisation$msg_abos), à l'ordre d'Aquilenet, à

Aquilenet
20 rue Tourat
33000 BORDEAUX

Ou alors, pour que l'on puisse prélever automatiquement ce montant,
vous pouvez remplir un mandat, soit avec le formulaire en ligne
disponible à partir du bouton 'ajouter ou changer' de l'espace compta

https://adherents.aquilenet.fr/compta.php

soit avec le formulaire papier (non recommandé, à cause des erreurs
de recopie):

http://www.aquilenet.fr/docs/mandat.pdf

Merci !
Les trésorières et trésoriers", "Renouvellement d'adhésion à Aquilenet",
		array(), array(), array(),
		$adherent->login."@aquilenet.fr", "tresorier@aquilenet.fr");

	print("Rappel à ".$adherent->$dolibarr_prenom." ".$adherent->$dolibarr_nom." envoyé");

}

function anciens_adherents($db, $date, $sortfield, $sortorder) {
	global $dolibarr_prenom;
	global $dolibarr_nom;
	global $dolibarr_cotisation;
	global $conf;

	$sql = "SELECT d.rowid, d.$dolibarr_prenom as prenom, d.$dolibarr_nom as nom, d.societe, d.datefin, d.email, d.statut,";
	$sql.= " d.fk_adherent_type as type_id, ";
	$sql.= " t.libelle as type, t.$dolibarr_cotisation as cotisation ";
	$sql.= " FROM ".MAIN_DB_PREFIX."adherent as d, ".MAIN_DB_PREFIX."adherent_type as t";
	$sql.= " WHERE d.fk_adherent_type = t.rowid ";
	$sql.= " AND d.entity = ".$conf->entity;
	$sql.= " AND d.datefin < '".$db->idate($date)."'";
	$sql.= $db->order($sortfield,$sortorder);

	return $sql;
}

$sortorder=$_GET["sortorder"];
$sortfield=$_GET["sortfield"];

if (! $sortorder) {  $sortorder="ASC"; }
if (! $sortfield) {  $sortfield="datefin,$dolibarr_nom"; }

$date = mktime(0, 0, 0, date("m"), date("t"), date("Y") - 3);
$sql = anciens_adherents($db, $date, $sortfield, $sortorder);

print '<p>Anciens adhérents, TODO: les supprimer proprement:</p>';
print '<table class="nobordernopadding" width="100%">';
print '<tr class="liste_titre">';
print_liste_field_titre("Prénom","cotisations.php","$dolibarr_prenom","","","",$sortfield,$sortorder);
print_liste_field_titre("Nom","cotisations.php","$dolibarr_nom","","","",$sortfield,$sortorder);
print_liste_field_titre("Email","cotisations.php","email","","","",$sortfield,$sortorder);
print_liste_field_titre("Date Fin","cotisations.php","datefin","","","",$sortfield,$sortorder);
print '<td align="left">Type</td>';
print "</tr>\n";

$lastmonth = -1;
$var=true;
$resql = $db->query($sql);
if ($resql)
{
	$numr = $db->num_rows($resql);
	for ($i = 0; $i < $numr; $i++)
	{
		$var = !$var;
		$objp = $db->fetch_object($resql);

		if ($objp->statut != 0)
			continue;

		$adherent = new Adherent($db);
		$adherent->fetch($objp->rowid);

		$dateanniv = $adherent->datefin + 3600*24;
		$dateanniv_array = getdate($dateanniv);
		$month = $dateanniv_array[mon];

		print '<tr '.$bc[$var].'>';
		print '<td><a href='.DOL_URL_ROOT.'/adherents/card.php?rowid='.$objp->rowid.'>'.$adherent->$dolibarr_prenom.'</a></td>';
		print '<td><a href='.DOL_URL_ROOT.'/adherents/card.php?rowid='.$objp->rowid.'>'.$adherent->$dolibarr_nom.'</a></td>';
		print '<td><a href='.DOL_URL_ROOT.'/adherents/card.php?rowid='.$objp->rowid.'>'.$adherent->email.'</a></td>';
		print '<td><a href='.DOL_URL_ROOT.'/adherents/card_subscriptions.php?rowid='.$objp->rowid.'>'.dol_print_date($dateanniv,'day').'</a></td>';
		print '<td><a href='.DOL_URL_ROOT.'/adherents/type.php?rowid='.$adherent->typeid.'>'.$adherent->type.'</a></td>';
	}
}
print '</table>';



$date = mktime(0, 0, 0, date("m"), date("t"), date("Y"));

$sql = anciens_adherents($db, $date, $sortfield, $sortorder);

print '<p>Cotisations à renouveler:</p>';
print '<table class="nobordernopadding" width="100%">';
print '<tr class="liste_titre">';
print_liste_field_titre("Prénom","cotisations.php","$dolibarr_prenom","","","",$sortfield,$sortorder);
print_liste_field_titre("Nom","cotisations.php","$dolibarr_nom","","","",$sortfield,$sortorder);
print_liste_field_titre("Email","cotisations.php","email","","","",$sortfield,$sortorder);
print_liste_field_titre("Date Fin","cotisations.php","datefin","","","",$sortfield,$sortorder);
print '<td align="left">Type</td>';
print '<td align="left">Cotisation</td>';
print '<td align="left">Abonnements</td>';
print '<td align="left">RIB</td>';
print '<td align="left">Lignes</td>';
print '<td align="left" colspan="2">Actions</td>';
print "</tr>\n";

$lastmonth = -1;
$var=true;
$resql = $db->query($sql);
if ($resql)
{
	$numr = $db->num_rows($resql);
	for ($i = 0; $i < $numr; $i++)
	{
		$var = !$var;
		$objp = $db->fetch_object($resql);

		if ($objp->statut == 0)
			continue;

		$adherent = new Adherent($db);
		$adherent->fetch($objp->rowid);

		$dateanniv = $adherent->datefin + 3600*24;
		$dateanniv_array = getdate($dateanniv);
		$month = $dateanniv_array[mon];
		if ($month != $lastmonth)
		{
			print("<tr><td>==================</td></tr>");
			$lastmonth = $month;
		}

		print '<tr '.$bc[$var].'>';
		print '<td><a href='.DOL_URL_ROOT.'/adherents/card.php?rowid='.$objp->rowid.'>'.$adherent->$dolibarr_prenom.'</a></td>';
		print '<td><a href='.DOL_URL_ROOT.'/adherents/card.php?rowid='.$objp->rowid.'>'.$adherent->$dolibarr_nom.'</a></td>';
		print '<td><a href='.DOL_URL_ROOT.'/adherents/card.php?rowid='.$objp->rowid.'>'.$adherent->email.'</a></td>';
		print '<td><a href='.DOL_URL_ROOT.'/adherents/card_subscriptions.php?rowid='.$objp->rowid.'>'.dol_print_date($dateanniv,'day').'</a></td>';
		print '<td><a href='.DOL_URL_ROOT.'/adherents/type.php?rowid='.$adherent->typeid.'>'.$adherent->type.'</a></td>';

		$cotis = get_cotis($db, $adherent->id);
		print '<td><a href='.DOL_URL_ROOT.'/adherents/card_subscriptions.php?rowid='.$objp->rowid.'>'.$cotis.'</a></td>';

		$tiers = new Societe($db);
		$result = $tiers->fetch($adherent->fk_soc, '');

		$abos = somme_abos($db, $tiers);
		print '<td><a href='.DOL_URL_ROOT.'/contrat/liste.php?socid='.$tiers->id.'>'.$abos.'</a></td>';

		$account_adh = compte_adherent($db, $adherent);
		$rib = $tiers->display_rib();
		$compte="";
		if ($rib != "-" && $rib != "  -" && $rib != "Aucun BAN (RIB) d&eacute;fini" && $rib != "No BAN defined")
		{
			$compte = $rib;
		}
		else
		{
			$bac = new CompanyBankAccount($db);
			$bac->fetch(0, $tiers->id);
			$compte = $bac->iban;
		}
		if ($compte == "Aucun BAN (RIB) d&eacute;fini")
			$compte = "";
		if ($compte == "-")
			$compte = "";
		print '<td><a href='.DOL_URL_ROOT.'/compta/bank/account.php?account='.$account_adh->id.'>'.$compte.'</a></td>';
		print '<td>'.$adherent->$dolibarr_note.'</td>';

		print '<td>';
		if ($compte != "" || $cotis == 0)
		{
			print '<form name="charge" method="post" action=cotisations.php?rowid='.$objp->rowid.'>';
			print '<input type="hidden" name="action" value="charge">';
			print '<input type="submit" class="button" value="Charge">';
			print '</form>';
		}
		print '</td>';

		print '<td>';
		if (($compte == "" && $cotis != 0) || $adherent->email == "samuel.thibault@ens-lyon.org")
		{
			print '<form name="rappel" method="post" action=cotisations.php?rowid='.$objp->rowid.'>';
			print '<input type="hidden" name="action" value="rappel">';
			print '<input type="submit" class="button" value="Rappel">';
			print '</form>';
		}
		print "</td>\n";
	}
}
print '</table>';

$db->close();

?>
