<?php
/* Copyright (C) 2012-2015, 2017-2018 Samuel Thibault <samuel.thibault@ens-lyon.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

require_once("./pre.inc.php");
require_once("./baionet.lib.php");

// Security check
$result=restrictedArea($user,'banque');

$statut=isset($_GET["statut"])?$_GET["statut"]:'';
$rowid=isset($_GET["rowid"])?$_GET["rowid"]:$_POST["rowid"];

llxHeader();

$sql  = "SELECT b.rowid, b.label, b.amount";
$sql .= " FROM ".MAIN_DB_PREFIX."bank as b";
$sql .= " WHERE b.rowid >= 6000 AND b.rowid != 48138 and b.rowid != 48616";

$result =  $db->query($sql);
$num = $db->num_rows($result);
$total = 0;
print("$num lignes<br/>");
for ($i = 0; $i < $num; $i++) {
	$obj = $db->fetch_object($result);
	$total += $obj->amount;

	$sql  = "SELECT bu.url_id, bu2.url_id, b.rowid, b.amount, b.label";
	$sql .= " FROM ".MAIN_DB_PREFIX."bank_url as bu, ".MAIN_DB_PREFIX."bank_url as bu2, ".MAIN_DB_PREFIX."bank as b";

	$sql .= " WHERE bu.fk_bank=".$obj->rowid;
	$sql .= " AND bu.url_id = b.rowid";

	$sql .= " AND bu2.fk_bank = b.rowid";
	$sql .= " AND bu2.url_id = ".$obj->rowid;

	$result2 = $db->query($sql);
	if ($db->num_rows($result2) > 0) {
		$obj2 = $db->fetch_object($result2);
		if ($obj->amount != -$obj2->amount) {
			print("Incohérence ".$obj->amount." vs ".$obj2->amount." : <a href=/compta/bank/ligne.php?rowid=".$obj->rowid.">".$obj->label."</a> et ".
			      "<a href=/compta/bank/ligne.php?rowid=".$obj2->rowid.">".$obj2->label."</a><br/>");
		} else {
			#print($obj->rowid." OK<br/>");
		}
	} else {
		if ($obj->amount != 0) {
			print("Orphelin ".$obj->amount." <a href=/compta/bank/ligne.php?rowid=".$obj->rowid.">".$obj->label."</a><br/>");
		}
	}
}
print("Total: $total<br/>");

$db->close();

?>
