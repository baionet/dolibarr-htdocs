<?php
/* Copyright (C) 2012-2014, 2017-2020 Samuel Thibault <samuel.thibault@ens-lyon.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

require_once("./pre.inc.php");
require_once("./aquilenet.lib.php");

llxHeader();

?>

<ul>
<li><a href="check.php">Vérification somme lignes de compta</a>
</ul>
<ul>
<li><a href="adsl.php">Lignes ADSL</a>
<li><a href="cotisations.php">Cotisations</a>
<li><a href="contrats.php">Contrats</a>
</ul>
<ul>
<li><a href="nouvel_adherent.php">Nouvel adhérent⋅e</a>
<li><a href="nouvelle_souscription.php">Nouveau VPN ou ADSL</a>
<li><a href="nouveau_mandat.php">Nouveau mandat</a>
<li><a href="test_mail.php">Test envoi de mail</a>
</ul>
<ul>
<li><a href="check_balances.php">Vérification balances</a>
</ul>
<ul>
<li><a href="test_prelevements.php">Test prélèvements</a>
<li><a href="import_prelevements.php">Import prélèvements</a>
<li><a href="rappel_prelevements.php">Rappel prélèvements</a>
</ul>
<ul>
<li><a href="export_sous.php">Export sous.csv</a>
</ul>
<ul>
<li><a href="bilan.php">Bilan comptable actuel</a>
</ul>

<?php

$db->close();

?>
