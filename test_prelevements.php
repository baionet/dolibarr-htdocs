<?php
/* Copyright (C) 2012-2015, 2017 Samuel Thibault <samuel.thibault@ens-lyon.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

require_once("./pre.inc.php");
require_once("./aquilenet.lib.php");

// Security check
$result=restrictedArea($user,'banque');

llxHeader();

if ($_FILES['form']['name']) {
	print '<p>traitement de '.$_FILES['form']['name'].'</p>';
	test_prelevements($db, $user, $_FILES['form']['tmp_name']);
} else {
	print '<p><form method="post" action="test_prelevements.php" enctype="multipart/form-data">';
	print '<input type="file" name="form">';
	print '<input type=submit value="Envoyer"/>';
	print '</form></p>';
}

$db->close();

?>
