<?php
/* Copyright (C) 2012-2013, 2015, 2020 Samuel Thibault <samuel.thibault@ens-lyon.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

require_once("./pre.inc.php");
require_once("./aquilenet.lib.php");

// Security check
$result=restrictedArea($user,'banque');

$nom=isset($_GET["nom"])?$_GET["nom"]:$_POST["nom"];

llxHeader();

if ($_POST["action"] == "send")
{
	$tiers = new Societe($db);
	$result = $tiers->fetch('', $nom);
	if ($result <= 0) {
		printf("\n\n<p>Impossible de récupérer le tiers $nom</p>\n\n");
		$error++;
		dol_print_error($db,$tiers->error);
	}
	$adh = new Adherent($db);
	$reesult = $adh->fetch('', '', $tiers->id);
	if ($result <= 0) {
		printf("\n\n<p>Impossible de récupérer l'adhérent(e)</p>\n\n");
		$error++;
		dol_print_error($db,$tiers->error);
	}
	mymailadh($db, $adh, "Test d'envoi de mail",
		"Ceci est un simple test d'envoi de mail");
	print("<p>envoyé un mail à $nom</p>");
}

$html = new Form($db);
print "Indiquer le nom complet d'un adhérent";
print '<p><form method="post" action=test_mail.php>';
print '<input type="hidden" name="action" value="send">';
print '<input name="nom" value="">';
print '<input type="submit" class="button" value="envoyer">';
print '</form></p>';


$db->close();

?>
