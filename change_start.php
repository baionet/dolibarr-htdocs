<?php
/* Copyright (C) 2012-2013, 2015, 2020 Samuel Thibault <samuel.thibault@ens-lyon.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

require_once("./pre.inc.php");

// Security check
$result=restrictedArea($user,'banque');

$statut=isset($_GET["statut"])?$_GET["statut"]:'';
$rowid=isset($_GET["rowid"])?$_GET["rowid"]:$_POST["rowid"];

llxHeader();

if ($_POST["action"] == "update")
{
	//$db->begin();
	$dateval = dol_mktime(12,0,0,$_POST["datevmonth"],$_POST["datevday"],$_POST["datevyear"]);
	$sql = "UPDATE ".MAIN_DB_PREFIX."bank";
	$sql.= " SET datev = '".$db->idate($dateval)."'";
	$sql.= " WHERE fk_account = ".$rowid;
	$result = $db->query($sql);
	if ($result)
	{
		$db->commit();
	}
	else
	{
		$db->rollback();
		dol_print_error($db);
	}
}

$account = new Account($db);
$account->fetch($rowid);

$sql = "SELECT b.rowid, b.datev, b.label";
$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
$sql.= ", ".MAIN_DB_PREFIX."bank as b";
$sql.= " WHERE b.fk_account = ".$rowid;
$sql.= " AND b.fk_account = ba.rowid";
$sql.= " AND ba.entity = ".$conf->entity;
$result=$db->query($sql);
if ($result) {
	$num = $db->num_rows($result);
	$j = 0;
	while ($j < $num) {
		$line = $db->fetch_object($result);
		if (substr($line->label, 0, 3) == "FAS") {
			$start = $line->datev;
		}
		$end = $line->datev;
		$j++;
	}
}

$html = new Form($db);
print '<p>'.$account->label.'</p>';
print '<p>'.$account->comment.'</p>';
print "<p>Date actuelle: $start</p>";
print '<p><form name="update" method="post" action=change_start.php?rowid='.$rowid.'>';
print '<input type="hidden" name="action" value="update">';
print 'Nouvelle date:';
$html->select_date($db->jdate($start),'datev','', '', '', 'update');
print '<input type="submit" class="button" value="'.$langs->trans("Update").'">';
print '</form></p>';
print '<a href=adsl.php>Retour</a>';


$db->close();

?>
