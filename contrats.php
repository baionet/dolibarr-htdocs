<?php
/* Copyright (C) 2012-2015, 2017-2021 Samuel Thibault <samuel.thibault@ens-lyon.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

require_once("./pre.inc.php");
require_once("./aquilenet.lib.php");

// Security check
$result=restrictedArea($user,'banque');

$statut=isset($_GET["statut"])?$_GET["statut"]:'';
$rowid=isset($_GET["rowid"])?$_GET["rowid"]:$_POST["rowid"];

llxHeader();

if ($_POST["action"] == "charge")
{
	$sql = "SELECT c.rowid as cid, c.ref as ref,";
	$sql.= " s.rowid as socid, s.nom,";
	$sql.= " p.rowid as pid, p.label as label, p.duration as duration,";
	$sql.= " cd.rowid as cdid, cd.date_ouverture, cd.qty, cd.subprice as prix";
	$sql.= " FROM ".MAIN_DB_PREFIX."contrat as c,";
	$sql.= " ".MAIN_DB_PREFIX."societe as s,";
	$sql.= " ".MAIN_DB_PREFIX."contratdet as cd";
	$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."product as p ON cd.fk_product = p.rowid";
	$sql.= " WHERE";
	$sql.= " cd.rowid = ".$rowid;
	$sql.= " AND c.rowid = cd.fk_contrat";
	$sql.= " AND c.fk_soc = s.rowid";
	$sql.= " AND s.entity = ".$conf->entity;

	$resql = $db->query($sql);
	if ($resql)
	{
		$numr = $db->num_rows($resql);
		if ($numr != 1) {
			print("oops, plusieurs contrats! $numr\n");
		} else {
			$objp = $db->fetch_object($resql);

			$annuel = $objp->duration == "1y";

			switch ($objp->label)
			{
				case "Stockage disque cloud":
				case "Stockage disque NFS":
				case "Stockage disque local":
					$compte_produit = $COMPTE_PRODUIT_STOCKAGE;
					break;
				case "VPN":
				case "IPv4 fixe":
				case "VPN, préférentiel":
					$compte_produit = $COMPTE_PRODUIT_VPN;
					break;
				case "hebergement VM":
				case "VM avec disque local":
				case "hebergement mezzanine":
				case "Hébergement Cogent":
					$compte_produit = $COMPTE_PRODUIT_HEBERGEMENT;
					break;
				case "Accès Internet Wifi":
					$compte_produit = $COMPTE_PRODUIT_WIFI;
					break;
				case "Session BGP Cogent":
					$compte_produit = $COMPTE_PRODUIT_IP;
					break;
				case "Transit 1 an":
					$compte_produit = $COMPTE_PRODUIT_TRANSIT;
					break;
				default:
					$error++;
					print '<font color="red">';
					print "Contrat inconnu ".$objp->label.", je ne sais pas quel compte créditer";
					print '</font>';
					break;
			}
			$tiers = new Societe($db);
			if ($tiers->fetch($objp->socid, '') >= 1) {
				$adherent = new Adherent($db);
				if ($adherent->fetch('', '', $tiers->id) >= 1) {
					$id=$adherent->id;
					$compte = compte_adherent($db, $adherent);
				}
			}
			$last = get_contrat_last($compte->id, "Abonnement ".$objp->ref." ");

			$begin = $objp->date_ouverture;
			if ($db->jdate($last) < $db->jdate($begin) - 3600*24*2)
				// Probablement un ancien contrat dont on a repris l'identifiant
				$last = null;

			/* TODO: trouver où stocker proprement cette date de dernière facturation */
			if (!$last)
			{
				// Nouveau service, on le fait commencer à l'ouverture
				$last = $begin;
				$the_last = $db->jdate($last);
				$jour_last = date("d", $the_last);
				$mois_last = date("m", $the_last);
				$annee_last = date("Y", $the_last);
				if ($annuel)
					// service annualisé, on ne compte pas précisément les jours
					$prix = $objp->prix * $objp->qty;
				else {
					// service mensualisé, on compte précisément les jours
					$dernier_jour = date('t',mktime(0,0,0,$mois_last,1,$annee_last));
					$prix = my_round(($objp->prix * $objp->qty * ($dernier_jour-$jour_last+1)) / $dernier_jour);
					$jour_last = $dernier_jour;
				}
			}
			else if ($annuel)
			{
				// On va de date à date d'année en année
				$the_last = $db->jdate($last);
				$jour_last = date("d", $the_last);
				$mois_last = date("m", $the_last);
				$annee_last = date("Y", $the_last) + 1;
				$prix = $objp->prix * $objp->qty;
			}
			else
			{
				// On repart au début du mois
				$the_last = $db->jdate($last);
				$jour_last = 1;
				$mois_last = date("m", $the_last);
				$annee_last = date("Y", $the_last);
				// On passe au mois suivant
				$the_next = mktime(0, 0, 0, $mois_last, $jour_last, $annee_last) + 32*24*3600;
				$prix = $objp->prix * $objp->qty;

				// On facture à la fin du mois
				$jour_last = date("t", $the_next);
				$mois_last = date("m", $the_next);
				$annee_last = date("Y", $the_next);
			}

			// Date facture
			$date_facture = mktime(0, 0, 0, $mois_last, $jour_last, $annee_last);

			if ($annuel)
				$date = $annee_last;
			else
				$date = $mois_last ."/". $annee_last;

			//print("doing ".$objp->label." for ".$objp->nom." price ".$objp->prix * $objp->qty." from ".$jour_last."/".$mois_last."/".$annee_last);

			$db->begin();
			if (transfer($db, $compte->id, $compte_produit,$date_facture,"Abonnement ".$objp->ref." ".$objp->label." ".$objp->nom." $date", $prix, $user))
			{
				$error++;
			}

			if (! $error)
			{
				$db->commit();
				//print "--- end ok, echeance $mois_last $annee_last";
			}
			else
			{
				print '--- end error, code='.$error." echeance $mois_last $annee_last";
				$db->rollback();
			}
		}
	}
}

$sortorder=$_GET["sortorder"];
$sortfield=$_GET["sortfield"];

if (! $sortorder) {  $sortorder="ASC"; }
if (! $sortfield) {  $sortfield="label"; }

$sql = "SELECT c.rowid as cid, c.ref,";
$sql.= " s.rowid as socid, s.nom,";
$sql.= " p.rowid as pid, p.label as label, p.duration as duration,";
$sql.= " cd.rowid as cdid, cd.date_ouverture, cd.qty, cd.subprice as prix";
$sql.= " FROM ".MAIN_DB_PREFIX."contrat as c,";
$sql.= " ".MAIN_DB_PREFIX."societe as s,";
$sql.= " ".MAIN_DB_PREFIX."contratdet as cd";
$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."product as p ON cd.fk_product = p.rowid";
$sql.= " WHERE";
$sql.= " c.rowid = cd.fk_contrat";
$sql.= " AND c.fk_soc = s.rowid";
$sql.= " AND s.entity = ".$conf->entity;
$sql.= " AND cd.statut = 0";
$sql.= $db->order($sortfield,$sortorder);

$var=true;
$resql = $db->query($sql);
if ($resql)
{
	$numr = $db->num_rows($resql);
	if ($numr != 0) {
		print("Attention! Il y a des contrats encore à l'état de brouillons\n");
		print '<table>';

		$i = 0;
		while ($i < $numr)
		{
			$var = !$var;
			$objp = $db->fetch_object($resql);
			print '<tr '.$bc[$var].' id="'.$i.'">';
			print '<td><a href='.DOL_URL_ROOT.'/societe/card.php?socid='.$objp->socid.'>'.$objp->nom.'</a></td>';
			$tiers = new Societe($db);
			if ($tiers->fetch($objp->socid, '') >= 1) {
				$adherent = new Adherent($db);
				if ($adherent->fetch('', '', $tiers->id) >= 1) {
					$id=$adherent->id;
					$compte = compte_adherent($db, $adherent);
				}
			}
			# contrat
			$ref = $objp->ref;
			if ($ref == '') {
				$ref = $objp->cid;
			}
			print '<td><a href='.DOL_URL_ROOT.'/contrat/card.php?id='.$objp->cid.'>'.$ref.'</a></td>';

			# produit
			print '<td><a href='.DOL_URL_ROOT.'/product/card.php?id='.$objp->pid.'>'.$objp->label.'</a></td>';
			# quantité
			print '<td>'.$objp->qty.'</td>';
			# prix
			print '<td>'.number_format($objp->prix, 2, ',', ' ').' €</td>';
			# total
			print '<td>'.number_format($objp->qty * $objp->prix, 2, ',', ' ').' €</td>';

			$i++;
		}
		print '</table>';
	}
}

$sql = "SELECT c.rowid as cid, c.ref,";
$sql.= " s.rowid as socid, s.nom,";
$sql.= " p.rowid as pid, p.label as label, p.duration as duration,";
$sql.= " cd.rowid as cdid, cd.date_ouverture, cd.qty, cd.subprice as prix";
$sql.= " FROM ".MAIN_DB_PREFIX."contrat as c,";
$sql.= " ".MAIN_DB_PREFIX."societe as s,";
$sql.= " ".MAIN_DB_PREFIX."contratdet as cd";
$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."product as p ON cd.fk_product = p.rowid";
$sql.= " WHERE";
$sql.= " c.rowid = cd.fk_contrat";
$sql.= " AND c.fk_soc = s.rowid";
$sql.= " AND s.entity = ".$conf->entity;
$sql.= " AND cd.statut = 4";
$sql.= $db->order($sortfield,$sortorder);
$sql.= " , s.nom";

print '<table class="nobordernopadding" width="100%">';
print '<tr class="liste_titre">';
print_liste_field_titre("Adhérent(e)","stocakge.php","comment","","","",$sortfield,$sortorder);
print '<td align="left">Contrat</td>';
print '<td align="left">Produit</td>';
print '<td align="left">Quantité</td>';
print '<td align="left">Prix</td>';
print '<td align="left">Total</td>';
print '<td align="left">Par</td>';
print '<td align="left" width="80">début</td>';
print '<td align="left" width="80">last</td>';
print '<td align="left">Charge</td>';
print "</tr>\n";

$var=true;
$resql = $db->query($sql);
if ($resql)
{
	$numr = $db->num_rows($resql);
	$i = 0;
	while ($i < $numr)
	{
		$var = !$var;
		$objp = $db->fetch_object($resql);
		print '<tr '.$bc[$var].' id="'.$i.'">';
		print '<td><a href='.DOL_URL_ROOT.'/societe/card.php?socid='.$objp->socid.'>'.$objp->nom.'</a></td>';
		$tiers = new Societe($db);
		if ($tiers->fetch($objp->socid, '') >= 1) {
			$adherent = new Adherent($db);
			if ($adherent->fetch('', '', $tiers->id) >= 1) {
				$id=$adherent->id;
				$compte = compte_adherent($db, $adherent);
			}
		}
		# contrat
		$ref = $objp->ref;
		if ($ref == '') {
			$ref = $objp->cid;
		}
		print '<td><a href='.DOL_URL_ROOT.'/contrat/card.php?id='.$objp->cid.'>'.$ref.'</a></td>';

		# Est-ce qu'on facture un mois en retard?
		switch ($objp->label) {
			case "Accès Internet Wifi":
			case "VPN":
			case "VPN, préférentiel":
				$retard = 1;
				break;
			case "Stockage disque cloud":
			case "Stockage disque NFS":
			case "Stockage disque local":
			case "IPv4 fixe":
			case "hebergement VM":
			case "VM avec disque local":
			case "hebergement mezzanine":
			case "Hébergement Cogent":
			case "Session BGP Cogent":
			case "Transit 1 an":
				$retard = 0;
				break;
			default:
				$error++;
				print '<font color="red">';
				print "Contrat inconnu ".$objp->label.", je ne sais pas comment facturer";
				print '</font>';
				break;
		}

		# produit
		print '<td><a href='.DOL_URL_ROOT.'/product/card.php?id='.$objp->pid.'>'.$objp->label.'</a></td>';
		# quantité
		print '<td>'.$objp->qty.'</td>';
		# prix
		print '<td>'.number_format($objp->prix, 2, ',', ' ').' €</td>';
		# total
		print '<td>'.number_format($objp->qty * $objp->prix, 2, ',', ' ').' €</td>';
		# Par
		if ($objp->duration == '1m')
			print '<td>par mois</td>';
		else
			print '<td>par an</td>';
		# début
		$debut = substr($objp->date_ouverture, 0, 10);
		$the_debut = $db->jdate($debut);
		$jour_debut = date("d", $the_debut);
		$mois_debut = date("m", $the_debut);
		$annee_debut = date("Y", $the_debut);
		print '<td>'.$debut.'</td>';
		# fin
		$end = get_contrat_last($compte->id, "Abonnement ".$objp->ref." ");
		if ($db->jdate($end) < $db->jdate($debut) - 3600*24*2)
			// Probablement un ancien contrat dont on a repris l'identifiant
			$end = null;
		if (!$end) {
			if ($objp->duration == '1m')
				$end = date("Y-m-t", mktime(0, 0, 0, $mois_debut - $retard, 1, $annee_debut));
			else
				$end = date("Y-m-d", mktime(0, 0, 0, $mois_debut, 1, $annee_debut - 1));
			$the_end = $db->jdate($end);
			$end = 'Aucun';
		} else {
			$the_end = $db->jdate($end);
			if ($the_end < $the_debut)
				// Probablement un ancien contrat dont on a repris l'identifiant
				$the_end = $the_debut;
		}
		if ($objp->duration == '1m')
			$target_end = mktime(0, 0, 0, date("m") - $retard, 1, date("Y"));
		else
			$target_end = mktime(0, 0, 0, date("m")+1, 1, date("Y") - 1);
		print '<td><a href='.DOL_URL_ROOT.'/compta/bank/account.php?account='.$compte->id.'>';
		if ($target_end > $the_end)
		{
			print '<font color="red">';
		}
		print $end;
		if ($target_end > $the_end)
			print '</font>';
		print '</a></td>';
		#if ($target_end > $the_end) {
			print '<td>';
			print '<form name="charge" method="post" action=contrats.php?rowid='.$objp->cdid.'#'.$i.'>';
			print '<input type="hidden" name="action" value="charge">';
			print '<input type="submit" class="button" value="Charge">';
			print '</form>';
			print '</td>';
		#}
		# last
		print "</tr>\n";
		if (substr($objp->label, 0, 9) == 'Ancienne ') {
			print "<tr> <td>======</td> <td>======</td> <td>===</td> <td>=====</td> <td>======</td> <td>==</td> <td>====</td> <td>======</td> <td>======</td> <td>======</td> <td>======</td> <td>======</td></tr>";
		}
		$i++;
	}
}
print '</table>';

$db->close();

?>
