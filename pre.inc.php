<?php
/* Copyright (C) 2012, 2014, 2017, 2019-2020 Samuel Thibault <samuel.thibault@ens-lyon.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

require_once("../main.inc.php");
require_once(DOL_DOCUMENT_ROOT."/compta/bank/class/account.class.php");
require_once(DOL_DOCUMENT_ROOT."/societe/class/companybankaccount.class.php");
require_once(DOL_DOCUMENT_ROOT."/adherents/class/adherent.class.php");
if (stat(DOL_DOCUMENT_ROOT."/lib/admin.lib.php")) {
	require_once(DOL_DOCUMENT_ROOT."/lib/admin.lib.php");
} else {
	require_once(DOL_DOCUMENT_ROOT."/core/lib/admin.lib.php");
}

function llxHeader($head = '', $title='', $help_url='', $target='', $disablejs=0, $disablehead=0, $arrayofjs='', $arrayofcss='')
{
	global $db, $user, $conf, $langs;
	global $dolibarr_prenom;
	global $dolibarr_nom;

	top_htmlhead($head, $title, $disablejs, $disablehead, $arrayofjs, $arrayofcss);
	top_menu($head, $title, $target, $disablejs, $disablehead, $arrayofjs, $arrayofcss);

	$menu = new Menu();
	if ($user->rights->banque->lire)
	{
		$menu->add(DOL_URL_ROOT."/aquilenet/adsl.php","Lignes",0,$user->rights->banque->lire);
		#$sql = "SELECT rowid, label, courant";
		#$sql.= " FROM ".MAIN_DB_PREFIX."bank_account";
		#$sql.= " WHERE entity = ".$conf->entity;
		#$sql.= " AND clos = 0";
		#$sql.= " AND label LIKE 'Ligne%'";
		#$sql.= $db->order('label', 'ASC');
	
		#$resql = $db->query($sql);
		#if ($resql)
		#{
		#	$numr = $db->num_rows($resql);
		#	$i = 0;
		#	while ($i < $numr)
		#	{
		#		$objp = $db->fetch_object($resql);
		#		$menu->add_submenu(DOL_URL_ROOT."/adsl/ligne.php?id=".$objp->rowid,$objp->label, 1, $user->rights->banque->lire);
		#		$i++;
		#	}
		#}

		$menu->add(DOL_URL_ROOT."/adsl/cotisations.php","Cotisations",0,$user->rights->banque->lire);

		$sql = "SELECT rowid, $dolibarr_prenom as prenom, $dolibarr_nom as nom";
		$sql.= " FROM ".MAIN_DB_PREFIX."adherent";
		$sql.= " WHERE entity = ".$conf->entity;
		$sql.= $db->order("$dolibarr_nom", 'ASC');
	
		$resql = $db->query($sql);
		if ($resql)
		{
			$numr = $db->num_rows($resql);
			print $numr;
			$i = 0;
			while ($i < $numr)
			{
				$objp = $db->fetch_object($resql);
				#$menu->add_submenu(DOL_URL_ROOT."/adherents/card.php?rowid=".$objp->rowid,$objp->$dolibarr_prenom." ".$objp->$dolibarr_nom, 1, $user->rights->banque->lire);
				$i++;
			}
		}
	}
	left_menu($menu->liste, "", "", "");
}
?>
