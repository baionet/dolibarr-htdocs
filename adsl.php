<?php
/* Copyright (C) 2012-2015, 2017-2021 Samuel Thibault <samuel.thibault@ens-lyon.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

require_once("./pre.inc.php");
require_once("./baionet.lib.php");

// Security check
$result=restrictedArea($user,'banque');

$statut=isset($_GET["statut"])?$_GET["statut"]:'';
$rowid=isset($_GET["rowid"])?$_GET["rowid"]:$_POST["rowid"];

llxHeader();

if ($_POST["action"] == "charge")
{
	$ligne = get_adsl_ligne($rowid);

	$last = get_adsl_last($rowid);
	$the_begin = $db->jdate($last) + 25 * 24 * 3600;
	$mois_begin = date("m", $the_begin);
	$annee_begin = date("Y", $the_begin);
	$start = get_adsl_start($rowid);
	$the_start = $db->jdate($start);
	$jour_start = date("d", $the_start);
	$mois_start = date("m", $the_start);
	$annee_start = date("Y", $the_start);
	$jour_begin = "";
	if (!$last) {
		$annee_begin = $annee_start;
		$mois_begin = $mois_start;
		$jour_begin = $jour_start;
	}

	//print("charge $rowid $last $start $the_begin $ligne $annee_begin $mois_begin $jour_begin");
	$db->begin();
	$error = charge_adsl($ligne, $annee_begin, $mois_begin, $jour_begin);
	//$error++;

	if (! $error)
	{
		$db->commit();
		//print "--- end ok, ligne $ligne, echeance $mois_last $annee_last";
	}
	else
	{
		print '--- end error, code='.$error." ligne $ligne, echeance $mois_last $annee_last";
		$db->rollback();
	}

}

$sortorder=$_GET["sortorder"];
$sortfield=$_GET["sortfield"];

if (! $sortorder) {  $sortorder="ASC"; }
if (! $sortfield) {  $sortfield="label"; }

$sql = "SELECT rowid, label, comment, courant";
$sql.= " FROM ".MAIN_DB_PREFIX."bank_account";
$sql.= " WHERE entity = ".$conf->entity;
$sql.= " AND clos = 0";
$sql.= " AND label LIKE '%Ligne %'";
$sql.= $db->order($sortfield,$sortorder);

print '<table class="nobordernopadding" width="100%">';
print '<tr class="liste_titre">';
print_liste_field_titre("Ligne","adsl.php","label","","","",$sortfield,$sortorder);
print_liste_field_titre("Adhérent(e)","adsl.php","comment","","","",$sortfield,$sortorder);
print '<td align="left">Numéro</td>';
print '<td align="left">Débit</td>';
print '<td align="left">Tarif</td>';
print '<td align="left">Produit</td>';
print '<td align="left">Charge</td>';
print '<td align="left">IPv4</td>';
print '<td align="left">IPv6</td>';
print '<td align="left" width="80">début</td>';
print '<td align="left" width="80">last</td>';
print '<td align="left">Charge</td>';
print "</tr>\n";

$fin_ancienne = false;
$fin_resiliation = false;

$var=true;
$resql = $db->query($sql);
if ($resql)
{
	$numr = $db->num_rows($resql);
	$i = 0;
	while ($i < $numr)
	{
		$var = !$var;
		$objp = $db->fetch_object($resql);
		if (substr($objp->label, 0, 9) != 'Ancienne '
		       	and !$fin_ancienne) {
			print "<tr> <td>======</td> <td>======</td> <td>===</td> <td>=====</td> <td>======</td> <td>==</td> <td>====</td> <td>======</td> <td>======</td> <td>======</td> <td>======</td> <td>======</td></tr>";
			$fin_ancienne = true;
		}
		if (substr($objp->label, 0, 12) == 'Resiliation ' or 
			substr($objp->label, 0, 13) == 'Résiliation ' or
			substr($objp->label, 0, 9) == 'Nouvelle ' or
			substr($objp->label, 0, 8) == 'Reconstr'
		       	and !$fin_resiliation) {
			print "<tr> <td>======</td> <td>======</td> <td>===</td> <td>=====</td> <td>======</td> <td>==</td> <td>====</td> <td>======</td> <td>======</td> <td>======</td> <td>======</td> <td>======</td></tr>";
			$fin_resiliation = true;
		}
		#$menu->add_submenu(DOL_URL_ROOT."/adsl/ligne.php?id=".$objp->rowid,$objp->label, 1, $user->rights->banque->lire);
		print '<tr '.$bc[$var].'>';
		print '<td><a href='.DOL_URL_ROOT.'/compta/bank/card.php?id='.$objp->rowid.'>'.$objp->label.'</a></td>';
		$comment = $objp->comment;
		$comments = explode("\n", $comment);
		# Adhérent
		$nom = trim($comments[0]);
		$tiers = new Societe($db);
		$adherent = null;
		if ($tiers->fetch('', $nom) >= 1) {
			$adherent = new Adherent($db);
			if ($adherent->fetch('', '', $tiers->id) >= 1) {
				$id=$adherent->id;
			}
		}
		print '<td><a href='.DOL_URL_ROOT.'/adherents/card.php?rowid='.$id.'>'.$nom.'</a></td>';
		print '<td>'.$adherent->id.'</td>';
		# Débit
		print '<td>'.$comments[1].'</td>';
		# tarif
		print '<td>'.$comments[2].'</td>';
		# produit
		print '<td>'.number_format($comments[3], 2, ',', ' ').'</td>';
		# charge
		print '<td>'.number_format($comments[4], 2, ',', ' ').'</td>';
		# ipv4
		print '<td>'.$comments[5].'</td>';
		# ipv6
		print '<td>'.$comments[6].'</td>';
		# start
		$start = get_adsl_start($objp->rowid);
		$the_debut = intval($db->jdate($start));
		$jour_debut = date("d", $the_debut);
		$mois_debut = date("m", $the_debut);
		$annee_debut = date("Y", $the_debut);
		$end = get_adsl_last($objp->rowid);
		if ($end == '') {
			$end = date("Y-m-d", mktime(0, 0, 0, $mois_debut, 1, $annee_debut) - 1);
		}
		print '<td><a href=change_start.php?rowid='.$objp->rowid.'>'.$start.'</a></td>';
		$the_end = $db->jdate($end) + 25 * 24 * 3600;
		$target_end = mktime(0, 0, 0, date("m"), 1, date("Y"));
		print '<td>';
		if ($target_end > $the_end)
			print '<font color="red">';
		print $end;
		if ($target_end > $the_end)
			print '</font>';
		print '</td>';
		#if ($target_end > $the_end) {
			print '<td>';
			print '<form name="charge" method="post" action=adsl.php?rowid='.$objp->rowid.'>';
			print '<input type="hidden" name="action" value="charge">';
			print '<input type="submit" class="button" value="Charge">';
			print '</form>';
			print '</td>';
		#}
		# last
		print '</tr>';
		$i++;
	}
}
print '</table>';

$db->close();

?>
