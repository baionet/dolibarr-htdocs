<?php

# Copyright (c) 2011, 2013-2015, 2019 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require_once("/usr/share/dolibarr/htdocs/master.inc.php");
$langs->load("main");
@set_time_limit(0);
$result = $user->fetch('', 'admin');
if (! $result > 0) { dol_print_error('',$user->error); exit; }
$user->getrights();
require_once(DOL_DOCUMENT_ROOT."/compta/bank/class/account.class.php");

require_once("head.php");
require_once("baionet.lib.php");

# Imprime les lignes du compte ACCOUNT_REF, sauf les lignes de débit si
# ONLY_CREDIT est à true, et sauf les lignes de crédit si ONLY_DEBIT est à true
function print_account($db, $account_ref, $not_debit = false, $not_credit = false)
{
	$account = new Account($db);
	if ($account->fetch('', $account_ref) <= 0)
		return;

	#print("<h3 id=".$account_ref.">".$account_ref." ".$account->label."</h3>");
	if ($not_debit)
		print("<p>Par respect de la vie privée de nos adhérent(e)s, les lignes de débit ne sont pas affichées pour ce compte.</p>");
	if ($not_credit)
		print("<p>Par respect de la vie privée de nos adhérent(e)s, les lignes de crédit ne sont pas affichées pour ce compte.</p>");

	$account_id = $account->id;
        $sql = "SELECT b.rowid, b.datev as dv, b.label, b.amount";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank as b";
	$sql.= " WHERE b.fk_account=".$account_id;
	$sql.= $db->order("b.datev, b.datec", "DESC");
	$result = $db->query($sql);
	if ($result) {
	    $num = $db->num_rows($result);
	    print("<table class='table table-striped table-condensed table-hover'>\n");
	    print("<tr><th>Date</th><th>Objet</th><th style='text-align:right'>Débit</th><th style='text-align:right'>Crédit</th><th style='text-align:right'>Solde</th></tr>\n");
	    print("<tr class='solde'><td></td><td>Solde ".strftime("%Y")."</td>");
	    print("<td></td><td></td><td style='text-align:right'>".money($account->solde(0))." €</td>");
	    print("</tr>");
	    $total = $account->solde(0);
	    for ($i = 0; $i < $num; $i++) {
	        $obj = $db->fetch_object($result);

		if ($obj->label == "(Solde initial)")
			continue;

		# Get link to related account
		$url = 0;
		$sql = "SELECT bu.url_id, ba.ref";
		$sql.= " FROM ".MAIN_DB_PREFIX."bank_url as bu, ".MAIN_DB_PREFIX."bank_account as ba, ".MAIN_DB_PREFIX."bank as b";
		$sql.= " WHERE bu.fk_bank=".$obj->rowid;
		$sql.= " AND bu.url_id = b.rowid";
		$sql.= " AND b.fk_account = ba.rowid";
		$result2 = $db->query($sql);
		if ($result2)
		{
		    $obj2 = $db->fetch_object($result2);
		    $url = $obj2->url_id;
		}

		if (!($not_debit && $obj->amount < 0) && !($not_credit && $obj->amount > 0))
		{
		    print("<tr class=");print(fmod($i,2)?"'even'":"'odd'");print(" id=".$obj->rowid.">");
		    print("<td>".dol_print_date($db->jdate($obj->dv))."</td>");
		    print("<td>");
		    if ($url)
			    print("<a href=#$url onclick=\"\$obj=\$('#detail-".$obj2->ref."'); if (!\$obj.hasClass('in')) { location.hash = '#' + $url; \$obj.on('shown.bs.collapse', function() { location.hash = '#' + $url; }); \$obj.collapse('show'); return false; } \">");
		    print($obj->label);
		    if ($url)
			    print("</a>");
		    print("</td>");
		    if ($obj->amount < 0) {
		        print("<td style='text-align:right'>".money($obj->amount)." €</td><td></td>");
		    } else {
		        print("<td></td><td style='text-align:right'>".money($obj->amount)." €</td>");
		    }
		    if (!$not_debit && !$not_credit)
		        print("<td style='text-align:right'>".money($total)." €</td>");
		    print("</tr>\n");
		}
	        $total = $total - $obj->amount;
	    }
	    print("</table>\n");
	}
}

function solde_head()
{
    print("<table class='table table-striped table-condensed table-hover'>\n");
    print("<tr><th>Compte</th><th style='text-align:right'>Solde</th></tr>\n");
}

function solde_tail()
{
    print("</table>");
}

# Imprime le solde des comptes de ACCOUNT_BEGIN à ACCOUNT_END, avec le détail en tableau déroulant
function soldes_accounts($db, $account_begin, $account_end)
{
	$sql = "SELECT ba.ref, ba.label";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
	$sql.= " WHERE ba.ref >= $account_begin and ba.ref <= $account_end";
	$sql.= $db->order("ba.ref", "ASC");
	$result = $db->query($sql);
	$total = 0;
	if ($result) {
	    $num = $db->num_rows($result);
	    for ($i = 0; $i < $num; $i++) {
		$obj = $db->fetch_object($result);
		$account = new Account($db);
		if ($account->fetch('', $obj->ref) <= 0)
			continue;
		$total = $total + $account->solde(0);
		print("<tr><td><button type=\"button\" class=\"btn btn-info\" data-toggle=\"collapse\" data-target=\"#detail-".$obj->ref."\">+</button> ".$obj->ref." ".$obj->label."</td>");
		print("<td style='text-align:right'>".money($account->solde(0))." €</td>");
		print("</tr><tr><td><div id=\"detail-".$obj->ref."\" class=\"collapse\">");

		print_account($db, $obj->ref,
			# pas de détail des paiements
			   $obj->ref == 51120000
			|| $obj->ref == 51190000
			# pas de détail du crédit coopératif
			|| $obj->ref == 51200001
			# pas de détail de caisse
			|| $obj->ref == 53140000
			# pas de détail des lignes ADSL
			|| $obj->ref == 60000000,
			# pas de détail des produits pour les adhérents
			$obj->ref >= 70000000 && $obj->ref <= 79999999 && $obj->ref != 76000000
			# pas de détail du crédit coopératif
			|| $obj->ref == 51200001
			# pas de détail de caisse
			|| $obj->ref == 53140000
			);

		print("</div></td></tr>");
	    }
	}
	return $total;
}

# Imprime le total des soldes des comptes de ACCOUNT_BEGIN à ACCOUNT_END, sans le détail en tableau déroulant
function solde_accounts($db, $account_begin, $account_end)
{
	$sql = "SELECT ba.ref, ba.label";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
	$sql.= " WHERE ba.ref >= $account_begin and ba.ref <= $account_end";
	$sql.= $db->order("ba.ref", "ASC");
	$result = $db->query($sql);
	$total = 0;
	if ($result) {
	    $num = $db->num_rows($result);
	    for ($i = 0; $i < $num; $i++) {
		$obj = $db->fetch_object($result);
		$account = new Account($db);
		if ($account->fetch('', $obj->ref) <= 0)
			continue;
		$total = $total + $account->solde(0);

		print("<tr><td>".$obj->ref." ".$obj->label."</td>");
		print("<td style='text-align:right'>".money($account->solde(0))." €</td>");
		print("</tr>");
	    }
	}
	return $total;
}

# Imprime le total des soldes des comptes de ACCOUNT_BEGIN à ACCOUNT_END, sans aucun détail
function solde_total_accounts($db, $account_begin, $account_end, $label)
{
	$sql = "SELECT ba.ref, ba.label";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
	$sql.= " WHERE ba.ref >= $account_begin and ba.ref <= $account_end";
	$sql.= $db->order("ba.ref", "ASC");
	$result = $db->query($sql);
	$total = 0;
	if ($result) {
	    $num = $db->num_rows($result);
	    for ($i = 0; $i < $num; $i++) {
		$obj = $db->fetch_object($result);
		$account = new Account($db);
		if ($account->fetch('', $obj->ref) <= 0)
			continue;
		$total = $total + $account->solde(0);
	    }
	    print("<tr class=even>");
	    print("<td>$account_begin - $account_end $label</td>");
	    print("<td style='text-align:right'>".money($total)." €</td>");
	    print("</tr>");
	}
	return $total;
}

$grand_total = 0;

print("<p>Le fonctionnement de la compta est détaillé sur <a href=https://wiki.baionet.fr/doku.php?id=compta>le wiki</a>.</p>");

print("<h2>Soldes des comptes</h2>");

print("<h3 id=bilans>Bilans</h3>");
solde_head();
    $grand_total += soldes_accounts($db, 10000001, 19999999);
solde_tail();

print("<h3 id=caisses>Caisses</h3><p>C'est l'argent dont on dispose. Attention, un solde négatif signifie que la banque nous doit des sous, pas le contraire :)</p>");
solde_head();
    $grand_total += soldes_accounts($db, 50000000, 59999999);
    $grand_total += soldes_accounts($db, 10000000, 10000000);
solde_tail();

print("<h3 id=charges>Charges</h3><p>C'est ce que l'on a payé pendant l'année en cours.");
solde_head();
    $grand_total += soldes_accounts($db, 60000000, 69999999);
solde_tail();

print("<h3 id=produits>Produits</h3>C'est ce que l'on a gagné pendant l'année en cours.");
solde_head();
    $grand_total += soldes_accounts($db, 70000000, 79999999);
solde_tail();

print("<h3 id=immobilisations>Immobilisations</h3>");
solde_head();
    $grand_total += soldes_accounts($db, 20000000, 29999999);
solde_tail();

print("<h3 id=stocks>Stocks</h3>");
solde_head();
    $grand_total += soldes_accounts($db, 30000000, 39999999);
solde_tail();

print("<h3 id=encours>Dettes et créances tiers</h3><p>Ce sont nos dettes (en positif) et créances (en négatif). Certains détails ne sont pas fournis par respect de la vie privée de nos adhérents.</p>");
solde_head();
    $grand_total += solde_total_accounts($db,100000000,999999999, "Lignes ADSL");
    $grand_total += soldes_accounts($db, 40100000, 40100003);
    $grand_total += solde_accounts($db, 40100004, 40100004);
    $grand_total += soldes_accounts($db, 40100005, 40999999);
    $grand_total += solde_total_accounts($db, 41000000, 41999999, "Adhérent(e)s");
    $grand_total += solde_total_accounts($db, 42000000, 49999999, "Tiers divers");
solde_tail();
print("<h3 id=total>Total</h3><p>Doit être nul !</p>");
solde_head();
	    print("<tr class=even>");
	    print("<td>Total</td>");
	    print("<td style='text-align:right'>".money($grand_total)." €</td>");
	    print("</tr>");
solde_tail();

require_once("tail.php");
?>
