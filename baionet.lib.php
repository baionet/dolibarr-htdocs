<?PHP

/*

Copyright (c) 2011-2015, 2017-2021 Samuel Thibault <samuel.thibault@ens-lyon.org>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

$dolibarr_version = preg_split('/[.-]/',DOL_VERSION);

require_once(DOL_DOCUMENT_ROOT."/lib/admin.lib.php");
require_once(DOL_DOCUMENT_ROOT."/compta/bank/class/account.class.php");
require_once(DOL_DOCUMENT_ROOT."/societe/class/companybankaccount.class.php");
require_once(DOL_DOCUMENT_ROOT."/adherents/class/adherent.class.php");
require_once(DOL_DOCUMENT_ROOT."/contrat/class/contrat.class.php");

# Probablement avant ça
if (versioncompare($dolibarr_version, array("10","0","0")) >= 0) {
  require_once(DOL_DOCUMENT_ROOT."/core/lib/date.lib.php");
} else {
  require_once(DOL_DOCUMENT_ROOT."/lib/date.lib.php");
}

set_include_path(get_include_path().PATH_SEPARATOR."/usr/share/php");
require_once("Crypt/GPG.php");

require_once("config.php");

$dolibarr_prenom = "firstname";
$dolibarr_nom = "lastname";
$dolibarr_adresse = "address";
$dolibarr_cp = "zip";
$dolibarr_ville = "town";
$dolibarr_cotisation = "subscription";
$dolibarr_note = "note_private";
$dolibarr_note_suffix = "_private";
$dolibarr_fetch_lines = "fetch_lines";
if (versioncompare($dolibarr_version, array("3","4","0")) < 0) {
	# old names
	$dolibarr_prenom = "prenom";
	$dolibarr_nom = "nom";
	$dolibarr_adresse = "adresse";
        $dolibarr_cp = "cp";
	$dolibarr_ville = "ville";
	$dolibarr_cotisation = "cotisation";
	$dolibarr_note = "note";
	$dolibarr_note_suffix = "note";
	$dolibarr_fetch_lines = "fetch_lignes";
}

/* Arrondi par défaut */
function my_round($price) {
	return round($price + 0.005, 2, PHP_ROUND_HALF_UP)-0.01;
}

function liste_vpns($db,$tiers) {
	global $conf;

	$sql = "SELECT c.rowid as cid, c.ref as ref,";
	$sql.= " s.rowid as socid, s.nom,";
	$sql.= " p.rowid as pid, p.label as label,";
	$sql.= " cd.rowid as cdid, cd.date_ouverture as ouverture, cd.qty, cd.subprice as prix";
	$sql.= " FROM ".MAIN_DB_PREFIX."contrat as c,";
	$sql.= " ".MAIN_DB_PREFIX."societe as s,";
	$sql.= " ".MAIN_DB_PREFIX."contratdet as cd";
	$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."product as p ON cd.fk_product = p.rowid";
	$sql.= " WHERE";
	$sql.= " s.rowid = ".$tiers->id;
	$sql.= " AND c.rowid = cd.fk_contrat";
	$sql.= " AND c.fk_soc = s.rowid";
	$sql.= " AND s.entity = ".$conf->entity;
	#$sql.= " AND cd.statut = 4";

	$contrats = array();
	$resql = $db->query($sql);
	if ($resql)
	{
		$numr = $db->num_rows($resql);
		for ($i = 0; $i < $numr; $i++) {
			$objp = $db->fetch_object($resql);
			if (substr($objp->ref,0,4) == 'vpn:')
				$contrats[] = $objp;
		}
	}
	return $contrats;
}

function somme_abos($db,$tiers) {
	global $conf;

	$sql = "SELECT c.rowid as cid, c.ref as ref,";
	$sql.= " s.rowid as socid, s.nom,";
	$sql.= " p.rowid as pid, p.label as label, p.duration as duration,";
	$sql.= " cd.rowid as cdid, cd.date_ouverture as ouverture, cd.qty, cd.subprice as prix";
	$sql.= " FROM ".MAIN_DB_PREFIX."contrat as c,";
	$sql.= " ".MAIN_DB_PREFIX."societe as s,";
	$sql.= " ".MAIN_DB_PREFIX."contratdet as cd";
	$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."product as p ON cd.fk_product = p.rowid";
	$sql.= " WHERE";
	$sql.= " s.rowid = ".$tiers->id;
	$sql.= " AND c.rowid = cd.fk_contrat";
	$sql.= " AND c.fk_soc = s.rowid";
	$sql.= " AND s.entity = ".$conf->entity;
	$sql.= " AND cd.statut = 4";

	$somme = 0;
	$resql = $db->query($sql);
	if ($resql)
	{
		$numr = $db->num_rows($resql);
		for ($i = 0; $i < $numr; $i++) {
			$objp = $db->fetch_object($resql);
			if ($objp->duration == "1y")
				$somme = $somme + $objp->qty * $objp->prix;
			else
				$somme = $somme + 12 * $objp->qty * $objp->prix;
		}
	}
	return $somme;
}

/* Trouver un transfer à une date donnée */
function get_transfer($db,$from,$to,$date,$user) {
	global $conf;

	if (!$from || !$to) {
		print("id de compte ($from, $to) invalide !\n");
		return -1;
	}

	$res = -1;
	$datec = "";

	$date = date('Y-m-d', $date);

	$sql = "SELECT b.amount, b.datec";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
	$sql.= ", ".MAIN_DB_PREFIX."bank_account as ba2";
	$sql.= ", ".MAIN_DB_PREFIX."bank as b";
	$sql.= ", ".MAIN_DB_PREFIX."bank as b2";
	$sql.= ", ".MAIN_DB_PREFIX."bank_url as bu";
	$sql.= " WHERE b.fk_account = ".$from;
	$sql.= " AND b2.fk_account = ".$to;

	$sql.= " AND ((bu.fk_bank = b.rowid";
	$sql.= " AND bu.url_id = b2.rowid)";
	$sql.= " OR (bu.fk_bank = b2.rowid";
	$sql.= " AND bu.url_id = b.rowid))";

	$sql.= " AND ba.rowid = ".$from;
	$sql.= " AND ba.entity = ".$conf->entity;

	$sql.= " AND ba2.rowid = ".$to;
	$sql.= " AND ba2.entity = ".$conf->entity;

	$sql.= " AND b.datev like '%$date%'";
	$result=$db->query($sql);
	if ($result) {
		$num = $db->num_rows($result);
		$j = 0;
		while ($j < $num) {
			$line = $db->fetch_object($result);
			$res = $line->amount;
			$datec = $line->datec;
			$j++;
		}
	}
	return [ $res, $datec ];
}

/* Effectuer un transfert depuis un compte vers un autre */
function transfer($db,$from,$to,$date,$intitule,$amount,$user) {
	if (!$from || !$to) {
		print("id de compte ($from, $to) invalide !\n");
		return -1;
	}
	/* Récupérer le compte source */
	$accountfrom = new Account($db);
	if ($accountfrom->fetch($from) < 1) {
		dol_print_error($db,$accountfrom->error);
		return -1;
	}
	/* Récupérer le compte destination */
	$accountto = new Account($db);
	if ($accountto->fetch($to) < 1) {
		dol_print_error($db,$accountto->error);
		return -1;
	}
	/* Ajout du débit */
	$from_id = $accountfrom->addline($date, "LIQ", $intitule, -$amount, '', '', $user);
	if ($from_id <= 0) {
		dol_print_error($db,$accountfrom->error);
		return -1;
	}
	/* Ajout du crédit */
	$to_id = $accountto->addline($date, "LIQ", $intitule, $amount, '', '', $user);
	if ($to_id <= 0) {
		dol_print_error($db,$accountto->error);
		return -1;
	}
	/* Ajout du lien entre les deux lignes */
	if ($accountfrom->add_url_line($from_id,$to_id, DOL_URL_ROOT.'/compta/bank/ligne.php?rowid=', '(banktransfert)', 'banktransfert') <= 0) {
		dol_print_error($db,$accountfrom->error);
		return -1;
	}
	if ($accountto->add_url_line($to_id,$from_id, DOL_URL_ROOT.'/compta/bank/ligne.php?rowid=', '(banktransfert)', 'banktransfert') <= 0) {
		dol_print_error($db,$accountto->error);
		return -1;
	}
	return 0;
}

/* Récupère le compte d'un adhérent */
function compte_adherent($db, $adh) {
	$account = new Account($db);
	if ($account->fetch("", 41100000 + $adh->id) < 1) {
		print ("\n\nerreur récupération compte pour l'adhérent $adherent (".$adh->id.")\n\n");
		dol_print_error($db,$account_adh->error);
		return null;
	}
	return $account;
}

/* Prélever un montant sur le compte d'un utilisateur: fait le transfert dans
 * dolibarr, et génère un fichier TeX pour générer l'ordre de virement à
 * faxer.  */
function prelevement($db,$user,$nom,$amount,$objet) {
	$root_si = "/srv/si";
	$formulaires = $root_si."/formulaires";

	/* Récupérer le tiers */
	$tiers = new Societe($db);
	if ($tiers->fetch('', $nom) < 1) {
		dol_print_error($db,$tiers->error);
		return -1;
	}
	$adherent = new Adherent($db);
	if ($adherent->fetch('', '', $tiers->id) < 1) {
		dol_print_error($db,$adherent->error);
		return -1;
	}
	/* Récupérer son RIB */
	$account = new CompanyBankAccount($db);
	if ($account->fetch(0,$tiers->id) < 1) {
		dol_print_error($db,$account->error);
		return -1;
	}
	/* Récupérer le compte de compta */
	$compt_account = compte_adherent($db, $adherent);
	if (!$compt_account) {
		return -1;
	}
	global $COMPTE_COMPTE_COURANT;
	global $COMPTE_CHARGE_SERVICES_BANCAIRES;
	global $COMM_PRELVT;
	/* Transférer l'argent */
	if (transfer($db,$COMPTE_COMPTE_COURANT,$compt_account->id,mktime(0, 0, 0, date("m"), date("t"), date("Y")),$objet,$amount,$user)) {
		print("\n\nErreur prélèvement\n\n");
		return -1;
	}
	/* Payer les frais bancaires */
	if (transfer($db,$COMPTE_CHARGE_SERVICES_BANCAIRES,$COMPTE_COMPTE_COURANT,mktime(0, 0, 0, date("m"), date("t"), date("Y")),"COMM PRELVT",$COMM_PRELVT,$user)) {
		print("\n\nErreur commission prélèvement\n\n");
		return -1;
	}
	if (strlen($account->code_banque) != 5 ||
	    strlen($account->code_guichet) != 5 ||
	    strlen($account->number) != 11 ||
	    strlen($account->cle_rib) != 2) {
	    print("\n\nErreur code RIB: '".$account->code_banque.' '.$account->code_guichet.' '.$account->number.' '.$account->cle_rib."'\n\n");
	    return -1;
	}
	/* Générer le formulaire */
	$tex = fopen("ordre-".strtr($adherent->login, " ", "_").".tex","w");
	fprintf($tex,"\\input{ordre-head.inc}\n");
	fprintf($tex,"\\newcommand{\\nom}{".$nom."}\n");
	fprintf($tex,"\\newcommand{\\etablissement}{".$account->code_banque."}\n");
	fprintf($tex,"\\newcommand{\\guichet}{".$account->code_guichet."}\n");
	fprintf($tex,"\\newcommand{\\numcompte}{".$account->number."}\n");
	fprintf($tex,"\\newcommand{\\clerib}{".$account->cle_rib."}\n");
	fprintf($tex,"\\newcommand{\\dat}{".date("t/m/Y")."}\n");
	fprintf($tex,"\\newcommand{\\montant}{".$amount."}\n");
	fprintf($tex,"\\newcommand{\\motif}{".$objet."}\n");
	fprintf($tex,"\\input{ordre-tail.inc}\n");
	fclose($tex);
	//symlink($formulaires."/logo.pdf","logo.pdf");
	//system("pdflatex ordre.tex");
	return 0;
}

/* définir le RIB pour un compte */
function definir_rib($db,$user,$nom,
	$banque_nom,$banqueadr,$banqueadrbis,$banquecp,$banqueville,
	$code_banque,$code_guichet,$number,$cle_rib) {
	$tiers = new Societe($db);
	if ($tiers->fetch('', $nom) < 1) {
		dol_print_error($db,$tiers->error);
		return -1;
	}
	$account = new CompanyBankAccount($db);
	if ($account->fetch(0,$tiers->id) < 1) {
		$account->sockid = $tiers->id;
		$result = $account->create($user);
		if (!$result) {
			return -1;
		}
	}
	$account->bank = $banque_nom;
	$account->code_banque = $code_banque;
	$account->code_guichet = $code_guichet;
	$account->number = $number;
	$account->cle_rib = $cle_rib;
	$account->domiciliation = "$banque_nom\n$banqueadr\n$banqueadrbis\n$banquecp $banqueville";
	$account->proprio = $tiers->$nom;
	//$account->adresse_proprio = $tiers->$dolibarr_adresse;
	if (!$account->update($user)) {
		return -1;
	}
	return 0;
}

/* définir l'IBAN pour un compte */
function definir_iban($db,$user,$nom, $iban, $bic) {
	$tiers = new Societe($db);
	if ($tiers->fetch('', $nom) < 1) {
		print("\n\nImpossible de récupérer le tiers\n\n");
		dol_print_error($db,$tiers->error);
		return -1;
	}
	$account = new CompanyBankAccount($db);
	if ($account->fetch(0,$tiers->id) < 1) {
		$account->sockid = $tiers->id;
		$result = $account->create($user);
		if (!$result) {
			print("\n\nImpossible de créer le RIB\n\n");
			return -1;
		}
	}
	$account->code_banque = "";
	$account->code_guichet = "";
	$account->number = "-";
	$account->cle_rib = "";
	$account->iban = $iban;
	$account->iban_prefix = $iban;
	$account->bic = $bic;
	$account->proprio = $tiers->$nom;
	//$account->adresse_proprio = $tiers->$dolibarr_adresse;
	if (!$account->update($user)) {
		return -1;
	}
	return 0;
}

function get_tarif($db, $prodid) {
	global $dolibarr_version;

	# Probablement avant ça
	if (versioncompare($dolibarr_version, array("10","0","0")) >= 0) {
		$sql  = "SELECT pfp.price FROM ";
		$sql .= MAIN_DB_PREFIX."product_fournisseur_price as pfp";
		$sql .= " WHERE pfp.fk_product = ".$prodid;
	} else {
		$sql  = "SELECT pfp.price FROM ";
		$sql .= MAIN_DB_PREFIX."product_fournisseur as pf";
		$sql .= ", ".MAIN_DB_PREFIX."product_fournisseur_price as pfp";
		$sql .= " WHERE pf.rowid = pfp.fk_product_fournisseur";
		$sql .= " AND pf.fk_product = ".$prodid;
	}

	$resql=$db->query($sql);
	if ($db->num_rows($resql) != 1) {
		print("\n\ntrouvé ".$db->num_rows($resql)." tarifs pour '$prodid'!\n\n");
		return -1;
	}
	$objp = $db->fetch_object($resql);
	return $objp->price;
}

/* Crée une ligne adsl pour un adhérent */
function creer_adsl($db, $user, $adh, $nom_complet, $adsltel, $tarif, $option) {
	global $dolibarr_note;
	global $dolibarr_note_suffix;
	$compt_account = compte_adherent($db,$adh);

	/* Ajout du numéro de ligne dans les notes de l'adhérent */
	$note = $adh->$dolibarr_note;
	if (!$note || $note == "")
		/* Premier abonnement */
		$note = $adsltel;
	else
		/* Abonnement supplémentaire */
		$note .= "\n".$adsltel;
	$adh->update_note($note,$dolibarr_note_suffix);

	/* Récupération du tarif */
	if ($tarif == "preferentiel") {
		$suf="-P";
	} else {
		$suf="";
	}
	if (
	   (substr($option,0,4) == "512k")
	or (substr($option,0,5) == "512RE")
	) {
		$debit="5";
	} else if (
	   (substr($option,0,2) == "8M")
	or (substr($option,0,3) == "10M")
	or (substr($option,0,3) == "14M")
	or (substr($option,0,3) == "18M")
	or (substr($option,0,3) == "80M")
	) {
		$debit="8";
	} else {
		$debit="1";
	}
	if (substr($option,-4) == "opt1") {
		// dégroupé
		$degroup="-D";
	} else if (substr($option,-4) == "opt3" || substr($option,-4) == "opt5") {
		$degroup="-ND";
	} else if (substr($option,-11) == "LiazoOrange") {
		$degroup="-L";
		$debit="8";
	} else if (substr($option,-13) == "LiazoOrangeNu") {
		$degroup="-LDT";
		$debit="8";
	} else {
		print("\n\nOption inconnue $option\n\n");
		return -1;
	}

        /* Nom final des produits dont on va chercher les tarifs dans dolibarr
        */
	$fas = "FAS-ADSL$degroup$suf";
	$mensuel = "ADSL$degroup$debit$suf";
	echo "ligne '$adsltel': facturation '$fas' et '$mensuel'\n";

	$prod_fas = new Product($db);
	$prod_fas->fetch(0,$fas);

	$prix_fdn_fas = get_tarif($db, $prod_fas->id);
	if ($prix_fdn_fas < 0) {
		return -1;
	}
	print("\n\npour $fas, trouvé $prix_fdn_fas\n\n");

	$prod = new Product($db);
	$prod->fetch(0,$mensuel);
	$prix_fdn_mensuel = get_tarif($db, $prod->id);
	if ($prix_fdn_mensuel < 0) {
		return -1;
	}
	print("\n\npour $mensuel, trouvé $prix_fdn_mensuel\n\n");

	print("\n\ntarif: $prix_fdn_fas ".$prod_fas->price_ttc." $prix_fdn_mensuel ".$prod->price_ttc."\n\n");

	/*
	 * Créer un compte de compta pour la ligne
	 */
	$compt_account_adsl = new Account($db, 0);
	$compt_account_adsl->ref = $adsltel;
	$compt_account_adsl->label = "Ligne ".$adsltel;
	$compt_account_adsl->courant = 2;
	$compt_account_adsl->clos = 0;
	$compt_account_adsl->rappro = 0;
	$compt_account_adsl->currency_code = "EUR";
	$compt_account_adsl->fk_pays = 1;
	$compt_account_adsl->country_id = 1;
	$compt_account_adsl->fk_departement = 0;
	$compt_account_adsl->date_solde = dol_now();
	$compt_account_adsl->comment = "$nom_complet"."\n".$option."\n".$tarif."\n".$prod->price_ttc."\n".$prix_fdn_mensuel;

	$id = $compt_account_adsl->create($user);
	if ($id <= 0) {
		print("\n\n erreur création compte\n\n");
		dol_print_error($db,$compt_account_adsl->error);
		return -1;
	}

	# Comptabilité FAS
	global $COMPTE_CHARGE_ADSL, $COMPTE_FOURNISSEUR_FDN;
	if (transfer($db,$COMPTE_CHARGE_ADSL,$compt_account_adsl->id,time(),"FAS Ouverture ligne ADSL $adsltel",$prix_fdn_fas,$user)) {
		print("\n\nerreur facturation FDN\n\n");
		return -1;
	}
	if (transfer($db,$compt_account_adsl->id,$COMPTE_FOURNISSEUR_FDN,time(),"FAS Ouverture ligne ADSL $adsltel",$prix_fdn_fas,$user)) {
		print("\n\nerreur facturation FDN\n\n");
		return -1;
	}
	global $COMPTE_PRODUIT_ADSL;
	if (transfer($db,$compt_account_adsl->id,$COMPTE_PRODUIT_ADSL,time(),"FAS Ouverture ligne ADSL $adsltel",$prod_fas->price_ttc,$user)) {
		print("\n\nerreur facturation ligne\n\n");
		return -1;
	}
	if (transfer($db,$compt_account->id,$compt_account_adsl->id,time(),"FAS Ouverture ligne ADSL $adsltel",$prod_fas->price_ttc,$user)) {
		print("\n\nerreur facturation adhérent\n\n");
		return -1;
	}

	# C'est très bof, les factures récurrentes
	#$facture = new FactureRec($db);
	#$facture->mode_reglement_id=3; // prélèvement
	#$facture->socid = $tiers->socid;
	#$facture->addline($facture->facid,
	#   $prod->description,
	#   $prod->price,
	#   1,
	#   $prod->tva_tx,
	#   0,0,
	#   $prod->id,
	#   0,
	#   0,0,
	#   0,
	#   0,
	#   '',
	#   'TTC',
	#   $prod->price_ttc,
	#   $prod->type);

	#$id = $facture->create($user);
	#if ($id <= 0) {
	#	$error++;
	#	dol_print_error($db,$facture->error);
	#}

	return 0;
}

function ranpass($len = "8"){
	$pass = NULL;
	for($i=0; $i<$len; $i++) {
		$char = chr(rand(48,122));
		/* On évite volontairement i, l et 1 qui se confondent */
		while (!preg_match("/[a-hjkm-zA-HJ-NP-Z2-9]/", $char))
			$char = chr(rand(48,122));
		$pass .= $char;
	}
	return $pass;
}


/* Crée une connexion VPN pour un adhérent */
function creer_vpn($db, $user, $tiers, $adh, $nom_complet, $password, $date, $tarif) {
	global $dolibarr_note_suffix, $dolibarr_fetch_lines;
	$vpns = liste_vpns($db, $tiers);
	$max = -1;
	if ($vpns)
		foreach ($vpns as $vpn) {
			print("VPN existant: ".$vpn->ref."\n");
			if (substr($vpn->ref,0,4+strlen($adh->login)) != "vpn:".$adh->login)
			{
				print("\n\noops, vpn ".$vpn->ref." n'a pas le bon nom ??\n\n");
				return null;
			}
			$tail = substr($vpn->ref,4+strlen($adh->login));
			if (!$tail)
				$max = 0;
			else 
			{
				if ($tail[0] != '.')
				{
					print("\n\noops, vpn ".$vpn->ref." n'a pas le bon nom ??\n\n");
					return null;
				}
				$tail = substr($tail,1);
				if ($tail > $max)
					$max = $tail;
			}
		}
	if ($max == -1)
		$suff = "";
	else
		$suff = ".".($max + 1);
	$login = $adh->login.$suff;

	$contrat = new Contrat($db);

	$contrat->socid = $tiers->id;
	$contrat->commercial_signature_id = 1;
	$contrat->commercial_suivi_id = 1;
	$contrat->date_contrat = $date;
	$contrat->ref = "vpn:".$login;
	print("Création de ".$login."\n");
	$result = $contrat->create($user);
	if ($result <= 0) {
		print("\n\noops, création du contrat:\n\n");
		dol_print_error('',$contrat->error);
		return null;
	}
	$contrat->update_note($password,$dolibarr_note_suffix);

	/* Récupération du tarif */
	if ($tarif == "preferentiel") {
		$suf="-P";
	} else {
		$suf="";
	}
	$prod = new Product($db);
	$prod->fetch(0,"VPN".$suf);

	$contrat->addline("", $prod->price_ttc, 1, 0, 0, 0, $prod->id,0, mktime(), mktime() + 100*365*24*3600, 'TTC',$prod->price_ttc, 0);

	$result = $contrat->validate($user);
	if ($result <= 0) {
		print("\n\noops, validation du contrat:\n\n");
		dol_print_error('',$contrat->error);
		return null;
	}
	$lines = $contrat->$dolibarr_fetch_lines();
	$result = $contrat->active_line($user, $lines[0]->id, mktime());
	if ($result <= 0) {
		print("\n\noops, activation du contrat:\n\n");
		dol_print_error('',$contrat->error);
		return null;
	}

	# C'est très bof, les factures récurrentes
	#$facture = new FactureRec($db);
	#$facture->mode_reglement_id=3; // prélèvement
	#$facture->socid = $tiers->socid;
	#$facture->addline($facture->facid,
	#   $prod->description,
	#   $prod->price,
	#   1,
	#   $prod->tva_tx,
	#   0,0,
	#   $prod->id,
	#   0,
	#   0,0,
	#   0,
	#   0,
	#   '',
	#   'TTC',
	#   $prod->price_ttc,
	#   $prod->type);

	#$id = $facture->create($user);
	#if ($id <= 0) {
	#	$error++;
	#	dol_print_error($db,$facture->error);
	#}

	return $login;
}

function get_adsl_start($rowid)
{
	global $db, $user, $conf, $langs;

	$sql = "SELECT b.rowid, b.datev, b.label";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
	$sql.= ", ".MAIN_DB_PREFIX."bank as b";
	$sql.= " WHERE b.fk_account = ".$rowid;
	$sql.= " AND b.fk_account = ba.rowid";
	$sql.= " AND ba.entity = ".$conf->entity;
	$sql.= " AND b.label LIKE 'FAS%'";
	$result=$db->query($sql);
	if ($result) {
		$num = $db->num_rows($result);
		$j = 0;
		while ($j < $num) {
			$line = $db->fetch_object($result);
			$start = $line->datev;
			$j++;
		}
	}
	return $start;
}

function get_adsl_last($rowid)
{
	global $db, $user, $conf, $langs;

	$sql = "SELECT b.rowid, b.datev, b.label";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
	$sql.= ", ".MAIN_DB_PREFIX."bank as b";
	$sql.= " WHERE b.fk_account = ".$rowid;
	$sql.= " AND b.fk_account = ba.rowid";
	$sql.= " AND ba.entity = ".$conf->entity;
	$sql.= " AND b.label like 'Abonnement%'";
	$result=$db->query($sql);
	$end = null;
	if ($result) {
		$num = $db->num_rows($result);
		$j = 0;
		while ($j < $num) {
			$line = $db->fetch_object($result);
			if (!$end || $db->jdate($end) < $db->jdate($line->datev))
				$end = $line->datev;
			$j++;
		}
	}
	return $end;
}

function get_adsl_ligne($rowid)
{
	global $db, $user, $conf, $langs;

	$sql = "SELECT rowid, label";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account";
	$sql.= " WHERE rowid = ".$rowid;
	$result=$db->query($sql);
	if ($result) {
		$obj = $db->fetch_object($result);
		if (substr($obj->label, 0, 6) == "Ligne ")
			return substr($obj->label,6);
		else if (substr($obj->label, 0, 15) == "Ancienne Ligne ")
			return substr($obj->label,15);
		else {
			print("Oops, le nom du compte de ligne '".$obj->label."' n'est pas reconnu.");
			$error++;
			return null;
		}
	}
	return null;
}

function charge_adsl($ligne, $annee_ligne, $mois_ligne, $jour_ligne)
{
	global $db;
	global $user;
	global $COMPTE_CHARGE_ADSL, $COMPTE_FOURNISSEUR_FDN, $COMPTE_PRODUIT_ADSL;

	$error = 0;
	/* Récupérer le compte de la ligne */
	$account = new Account($db);
	if ($account->fetch('', $ligne) < 1) {
		print ("\n\nerreur récupération compte pour la ligne $ligne\n\n");
		dol_print_error('',$account->error);
		$error++;
	}

	$infos = explode("\n", $account->comment);
	$adherent = trim($infos[0]);
	$produit = trim($infos[3]);
	$charge = trim($infos[4]);
	$jours = "";

	/* Cas d'une nouvelle construction: on facture au prorata */
	if ($jour_ligne) {
		$dernier = date('t',mktime(0, 0, 0, $mois_ligne, 1, $annee_ligne));
		$nb_jours = $dernier - $jour_ligne + 1;
		$produit = my_round(((float) $produit) * $nb_jours / $dernier);
		$charge = my_round(((float) $charge) * $nb_jours / $dernier);
		$jours = "$nb_jours sur $dernier";
	}

	/* Récupérer le compte de l'adhérent */
	$tiers = new Societe($db);
	if ($tiers->fetch('', $adherent) <= 0) {
		print("\n\nerreur récupération tiers ($adherent)\n\n");
		dol_print_error($db,$tiers->error);
		$error++;
	}
	$adh = new Adherent($db);
	if ($result = $adh->fetch('', '', $tiers->id) <= 0) {
		print ("\n\nerreur récupération adhérent $adherent (".$tiers->id.")\n\n");
		dol_print_error($db,$adh->error);
		$error++;
	}

	$account_adh = compte_adherent($db, $adh);
	if (!$account_adh) {
		$error++;
	}

	/* On est facturé à la fin du mois */
	/*  TODO jour ouvré ? apff */
	$time = mktime(0, 0, 0, $mois_ligne, 1, $annee_ligne);
	$annee = date('Y',$time);
	$mois = date('n',$time);
	$dernier = date('t',$time);
	$echeance = mktime(0, 0, 0, $mois, $dernier, $annee);

	/* Transfert de la charge */
	if (transfer($db,$COMPTE_CHARGE_ADSL,$account->id,$echeance,"Abonnement ligne $ligne $mois_ligne/$annee_ligne", $charge, $user)) {
		print("\n\n erreur charge ligne\n\n");
		$error++;
	}

	if (transfer($db,$account->id,$COMPTE_FOURNISSEUR_FDN,$echeance,"Abonnement ligne $ligne $mois_ligne/$annee_ligne", $charge, $user)) {
		print("\n\n erreur charge FDN\n\n");
		$error++;
	}

	/* Transfert du produit */
	if (transfer($db,$account->id,$COMPTE_PRODUIT_ADSL,$echeance,"Abonnement ligne $ligne $mois_ligne/$annee_ligne", $produit, $user)) {
		print("\n\n erreur produit ligne\n\n");
		$error++;
	}

	if (transfer($db,$account_adh->id,$account->id,$echeance,"Abonnement ligne $ligne $mois_ligne/$annee_ligne", $produit, $user)) {
		print("\n\n erreur produit adhérent\n\n");
		$error++;
	}

	return $error;
}

function get_contrat_last($rowid, $id)
{
	global $db, $user, $conf, $langs;

	$sql = "SELECT b.rowid, b.datev, b.label";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
	$sql.= ", ".MAIN_DB_PREFIX."bank as b";
	$sql.= " WHERE b.fk_account = ".$rowid;
	$sql.= " AND b.fk_account = ba.rowid";
	$sql.= " AND ba.entity = ".$conf->entity;
	$sql.= " AND b.label like '$id%'";
	$result=$db->query($sql);
	$end = null;
	if ($result) {
		$num = $db->num_rows($result);
		$j = 0;
		while ($j < $num) {
			$line = $db->fetch_object($result);
			if (!$end || $db->jdate($end) < $db->jdate($line->datev))
				$end = $line->datev;
			$j++;
		}
	}
	return $end;
}

function mymail($to, $subject, $message, $headers = "From: Tresoriers de Baionet <baionetreso@framalistes.org>", $parameters="")
{
	$headers.="\nContent-Type: text/plain; charset=UTF-8".
		  "\nContent-Transfer-Encoding: 8bit";
	mail($to, $subject, $message, $headers, $parameters);
}

# Comme mymail, mais le destinataire ($adh) est un adhérent
function mymailadh($db, $adh, $subject, $message, $headers = "From: Tresoriers de Baionet <baionetreso@framalistes.org>", $parameters="")
{
	$tiers = new Societe($db);
	$tiers->fetch($adh->fk_soc, '');
	$fingerprint=
	 	 $tiers->idprof1
		.$tiers->idprof2
		.$tiers->idprof3
		.$tiers->idprof4;
	if ($fingerprint) {
		system("sudo -u www-data /usr/bin/gpg --keyserver keys.gnupg.net --homedir /srv/adherents/.gnupg/ --recv-key $fingerprint");
		#echo("trouvé fingerprint $fingerprint\n");
		$home="/srv/adherents/.gnupg";
		$res = new Crypt_GPG(["homedir"=>$home]);
			#,"debug"=>true]);
		try {
			$res->addEncryptKey($fingerprint);
			mymail($adh->email, $subject, $res->encrypt($message), $headers, $parameters);
		} catch (Exception $e) {
			print("oops, pas trouvé la clé $fingerprint, mail non envoyé à ".$adh->email."... <br/>\n");
			mymail($adh->email, $subject,
"Bonjour,

nous avons essayé de vous envoyer un mail, mais nous n'avons pas pu 
rouver la clé 

$fingerprint

sur le réseau de serveurs pgp, nous avons essayé le keys.gnupg.net
sans l'y trouver. Pour corriger cela, veuillez envoyer la clé sur ce
serveur, ou nous envoyer votre clé par mail pour que nous l'injections
à la main.

Associativement,
La trésorerie", $headers, $parameters);
		}
	} else {
		mymail($adh->email, $subject, $message, $headers, $parameters);
	}
}

function money($amount)
{
	return str_replace(" ", "&nbsp;", number_format($amount, 2, ',', ' '));
}

function note_nouvel_adsl($prenom, $nom, $mail) {
	mymail("sympa@listes.aquilenet.fr", "", "subscribe adsl $prenom $nom", "From: $mail");
	mymail("collegiale@aquilenet.fr", "Nouvel abonnement ADSL", "Et un(e) abonné(e) de plus à l'ADSL: $prenom $nom, ça en fait ".compte_adsl($db)." en tout", "From: tresorier@aquilenet.fr");
}

function note_nouveau_vpn($db, $adh, $login, $prenom, $nom, $mail) {
	mymail("sympa@listes.aquilenet.fr", "", "subscribe vpn $prenom $nom", "From: $mail");
	mymail("collegiale@aquilenet.fr", "Nouvel abonnement VPN", "Et un(e) abonné(e) de plus au VPN: $prenom $nom, ça en fait ".compte_vpn($db)." en tout", "From: tresorier@aquilenet.fr");
	mymailadh($db, $adh, "Votre commande VPN est achevee",
	"Bonjour,

Votre commande de VPN est achevée. Vous pouvez en lire le login et mot
de passe sur

https://adherents.aquilenet.fr/ (ou bien via le lien depuis le site
web principal) avec le login

$login

et le mot de passe que vous avez choisi.

Pour l'instant l'adresse IP que vous obtiendrez sera dynamique, mais
vous pouvez demander une adresse IP fixe pour 2€/an, il suffit d'en
faire la demande.

La documentation pour la configuration du VPN est disponible sur

https://atelier.aquilenet.fr/projects/aquilenet/wiki/Configuration_VPN

Les trésorières et trésoriers, pour la collegiale d'Aquilenet", "From: tresorier@aquilenet.fr");
}

function notifications($nomail, $login_vpn, $password) {
	if ($password)
	{
		// TODO: better salt
		$salt = openssl_random_pseudo_bytes(8);
		$ciphered = crypt($password, "$6$".bin2hex($salt));
		print("\n\n<p>à ajouter à /etc/openvpn/password sur iris et demeter :<br/> <tt>$login_vpn:$ciphered</tt></p>\n\n");
	}
	if ($nomail)
	{
		printf("\n\n<p><b> *** ATTENTION ***<br/>Aucune adresse mail n'a été indiquée, il faut lui envoyer un courier papier</p>\n\n");
	}
}

function nouvel_adherent($db, $user, $file)
{
	global $dolibarr_prenom;
	global $dolibarr_nom;
	global $dolibarr_ville;
	$error = 0;
	$error_creation = 0;

	$f = fopen($file,'r');
	if (!$f)
		$error++;
	fseek($f, 0, SEEK_END);
	$len = ftell($f);
	fseek($f, 0);
	$data = fread($f, $len);

	$valeurs = explode("\n", $data);

	$num=0;
	$personne = $valeurs[$num++];
	$prenom = $valeurs[$num++];
	$nom = $valeurs[$num++];
	$adr = $valeurs[$num++];
	$adrbis = $valeurs[$num++];
	$cp = $valeurs[$num++];
	$ville = $valeurs[$num++];
	$tel = $valeurs[$num++];
	$mail = $valeurs[$num++];
	$passwd = $valeurs[$num++];
	#$njour = $valeurs[$num++];
	#$nmois = $valeurs[$num++];
	#$nannee = $valeurs[$num++];
	$njour = 01;
	$nmois = 01;
	$nannee = 1970;
	$iban = $valeurs[$num++];
	$bic = $valeurs[$num++];
	$dat = $valeurs[$num++];
	$lieu = $valeurs[$num++];
	$adsltel = $valeurs[$num++];
	$tarif = $valeurs[$num++];
	$option = $valeurs[$num++];
	$rum = $valeurs[$num++];
	$vpn = $valeurs[$num++];
	$pgp = $valeurs[$num++];

	echo "\n\n<p>enregistrement de $prenom $nom</p>\n\n";

	$jour=substr($dat,0,2);
	$mois=substr($dat,3,2);
	$annee=substr($dat,6);
	$date_fact = mktime(0, 0, 0, $mois, $jour, $annee);
	$date_nais = mktime(0, 0, 0, $nmois, $njour, $nannee);

	/* Virer les accents et majuscules */
	function sanitize($str) {
	  setlocale(LC_ALL, "fr_FR.UTF-8");
	  $str = iconv("UTF-8", "ASCII//TRANSLIT", $str);
	  $str = strtolower($str);
	  return $str;
	}

	$login = $prenom;
	if ($prenom && $nom)
	  $login .= ".";
	$login .= $nom;

	$login = sanitize($login);
	$tr =     "\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f".
	      "\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f".
	      "\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f".
	      "\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f".
	      "\x40".
	                                                  "\x5b\x5c\x5d\x5e\x5f".
	      "\x60".
	                                                  "\x7b\x7c\x7d\x7e\x7f";
	$dots = str_repeat('.', strlen($tr));
	$login = strtr($login, $tr, $dots);
	$login = preg_replace("/\.\.*/", ".", $login);

	$nom_complet = $prenom;
	if ($prenom && $nom)
	  $nom_complet .= " ";
	$nom_complet .= $nom;

	echo "\n\n<p>login $login</p>\n\n";

	// Start of transaction
	$db->begin();

	require_once(DOL_DOCUMENT_ROOT."/adherents/class/adherent.class.php");
	require_once(DOL_DOCUMENT_ROOT."/adherents/class/adherent_type.class.php");
	require_once(DOL_DOCUMENT_ROOT."/adherents/class/cotisation.class.php");
	require_once(DOL_DOCUMENT_ROOT."/user/class/user.class.php");
	require_once(DOL_DOCUMENT_ROOT."/compta/bank/class/account.class.php");
	require_once(DOL_DOCUMENT_ROOT."/societe/class/companybankaccount.class.php");
	require_once(DOL_DOCUMENT_ROOT."/product/class/product.class.php");
	require_once(DOL_DOCUMENT_ROOT."/compta/facture/class/facture-rec.class.php");
	require_once(DOL_DOCUMENT_ROOT."/aquilenet/aquilenet.lib.php");

	/*
	 * Créer l'adhérent(e)
	 */

	$adh = new Adherent($db);


	$adh->login = $login;
	if (!$passwd) {
	  print("\n\n<p>Mot de passe requis !</p>\n\n");
	  $error++;
	}

	if ($mail == "") {
	  print "\n\n<h1><b>Attention</b> l'adhérent n'a pas fourni d'adresse mail pour le prévenir de l'adhésion, il faut le faire à la main...</h1>\n\n";
	  $mail = "$login@baionet.fr";
	}

	$adh->pass = $passwd;
	$adh->$dolibarr_prenom = $prenom;
	$adh->$dolibarr_nom = $nom;
	$adh->societe = $nom;
	$adh->$dolibarr_adresse = $adr."\n".$adrbis;
	$adh->$dolibarr_cp = $cp;
	$adh->$dolibarr_ville = $ville;
	$adh->country_id = 1;
	$adh->country_code = "FR";
	$adh->country = "France";
	$adh->phone = $tel;
	$adh->email = $mail;
	//$adh->naiss = $date_nais;
	global $TYPEADH_PREFERENTIEL;
	global $TYPEADH_BIENFAITEUR;
	global $TYPEADH_ADHERENT;
	if ($tarif == 'preferentiel') {
		$adh->typeid = 2;
	} else if ($tarif == 'bienfaiteur') {
		$adh->typeid = 3;
	} else {
		$adh->typeid = 1; //$typeid;
	}
	if ($personne == "physique") {
		$adh->morphy = "phy";
	} else {
		$adh->morphy = "mor";
	}

	$result = $adh->create($user);
	if ($result <= 0) {
		$error++;
		print("\n\n<p>erreur création adhérent(e) $result</p>\n\n");
		$error_creation++;
		dol_print_error($db,$adh->error);
	}
	$result = $adh->validate($user);
	if ($result <= 0) {
		$error++;
		print("\n\n<p>erreur validation adhérent(e) $result</p>\n\n");
		$error_creation++;
		dol_print_error($db,$adh->error);
	}

	$id = $adh->id;

	/*
	 * Créer un tiers pour l'adhérent(e)
	 */
	$tiers = new Societe($db);
	$result = $tiers->create_from_member($adh,$nom_complet);
	if ($result < 0) {
		$error++;
		print("\n\n<p>erreur création tiers $result</p>\n\n");
		dol_print_error($db,$tiers->error);
	}

	if ($pgp) {
		$pgp = str_replace(" ","",$pgp);
		if (strlen($pgp) != 40)
		{
			printf("\n\n<p>La clé pgp ne fait pas 40 caractères</p>\n\n");
			$error++;
		}
		for ($i = 0; $i < strlen($pgp); $i++)
		{
			$c = $pgp[$i];
			if ($c >= 'a' && $c <= 'f')
				continue;
			if ($c >= 'A' && $c <= 'F')
				continue;
			if ($c >= '0' && $c <= '9')
				continue;
			printf("\n\n<p>La clé pgp contient un caractère bizarre à la position $i</p>\n\n");
			$error++;
		}
		if (!$error) {
			$ret = system("sudo -u www-data /usr/bin/gpg --keyserver keys.gnupg.net --homedir /srv/adherents/.gnupg/ --recv-key $pgp");
			if ($ret != 0)
			{
				printf("\n\n<p>Clé $pgp non trouvée sur keys.gnupg.net</p>\n\n");
				$error++;
			}
			$tiers->siren = substr($pgp, 0, 10);
			$tiers->siret = substr($pgp, 10, 10);
			$tiers->ape = substr($pgp, 20, 10);
			$tiers->idprof4 = substr($pgp, 30, 10);
			if (!$tiers->update($tiers->id, $user)) {
				printf("\n\n<p>Problème écriture clé PGP</p>\n\n");
				dol_print_error($db,$tiers->error);
				$error++;
			}
		}
	}

	/*
	 * Créer un rib pour l'adhérent(e)
	 */
	$account = new CompanyBankAccount($db);
	$account->socid = $tiers->id;
	$result = $account->create($user);
	if (!$result) {
		$error++;
		print("\n\n<p>erreur création rib</p>\n\n");
		dol_print_error($db,$account->error());
	}
	$account->number = "-";
	$account->iban_prefix = $iban;
	$account->iban = $iban;
	$account->bic = $bic;
	$account->proprio = $nom_complet;
	$account->adresse_proprio = "$adr\n$adrbis\n$cp $ville";

	$result = $account->update($user);
	if (!$result) {
		$error++;
		print("\n\n<p>erreur mise à jour compte</p>\n\n");
		dol_print_error($db,$account->error());
	}

	/*
	 * Créer un compte de compta pour l'adhérent(e)
	 */
	$compt_account = new Account($db, 0);
	$compt_account->ref = 41100000 + $id;
	$label = "Adhérent ".$nom_complet;
	$label = substr($label, 0, 30);
	$compt_account->label = $label;
	$compt_account->courant = 2;
	$compt_account->clos = 0;
	$compt_account->rappro = 0;
	$compt_account->currency_code = "EUR";
	$compt_account->fk_pays = 1;
	$compt_account->country_id = 1;
	$compt_account->fk_departement = 0;
	$compt_account->date_solde = dol_now();

	$id = $compt_account->create($user);
	if ($id <= 0) {
		$error++;
		print("\n\n<p>erreur création compte compta $id\n\n<p>");
		dol_print_error($db,$compt_account->error);
	}

	print("\n\n<p>$error erreurs pour l'instant</p>\n\n");

	/*
	 * Création de ligne ADSL
	 */

	print("\n\n<p>$error erreurs pour l'instant, maintenant ADSL</p>\n\n");

	if ($adsltel) {
		if (! $tarif) {
			print("\n\n<p>Le tarif choisi n'est pas spécifié dans le fichier</p>\n\n");
			$error++;
		}
		if (! $option) {
			print("\n\n<p>L'option n'est pas spécifiée dans le fichier</p>\n\n");
			$error++;
		}
		if (creer_adsl($db, $user, $adh, $nom_complet, $adsltel, $tarif, $option) < 0) {
			print("\n\n<p>erreur création ligne ADSL</p>\n\n");
			$error++;
		}
	}

	/*
	 * Création de connexion VPN
	 */

	print("\n\n<p>$error erreurs pour l'instant, maintenant VPN</p>\n\n");

	$password = "";
	$login_vpn = "";
	if ($vpn) {
		if (!$tarif) {
			$error ++;
			print("\n\n<p>le tarif n'est pas précisé dans le fichier</p>\n\n");
		}

		$password = ranpass(8);
		$login_vpn = creer_vpn($db, $user, $tiers, $adh, $nom_complet, $password, $date_fact, $tarif);
		if (!$login_vpn) {
			print("\n\n<p>erreur création connexion VPN</p>\n\n");
			$error++;
		}
	}

	// décommenter pour tester sans commiter dans dolibarr
	//$error++;

	// -------------------- END OF YOUR CODE --------------------

	if (! $error)
	{
		$db->commit();
		$nomail = 0;
		if ($mail == "")
		{
			print "\n\n<h1><b>Attention</b> l'adhérent n'a pas fourni d'adresse mail pour le prévenir de l'adhésion, il faut le faire à la main...</h1>\n\n";
			$nomail = 1;
			$mail = "$login@aquilenet.fr";
		}

		# +1 car ce(tte) nouvel(le) adhérent(e) n'est pas encore à jour de cotisation
		$num = compte_adherents($db)+1;
		mymail("sympa@framalistes.org", "", "subscribe asso $prenom $nom", "From: $mail");
		mymail("baionetreso@framalistes.org", "Nouvel adherent(te)", "Et un(e) adhérent(e) de plus: $prenom $nom , ça en fait $num en tout à jour de cotisation.", "From: baionetreso@framalistes.org");
		mymailadh($db, $adh, "Bienvenue a Baionet !",
	"Bonjour,

Votre adhésion à l'association a bien été validée, bienvenue
dans l'association Baionet !


$login

et le mot de passe que vous avez choisi.

Vous avez été abonné.e à la liste de diffusion

https://bayonnet@framalistes.org

Les trésorières et trésoriers de Baionet ", "From: baionetreso@framalistes.org");
		if ($adsltel && $tarif && $option) {
			note_nouvel_adsl($prenom, $nom, $mail);
		}
		if ($vpn && $tarif) {
			note_nouveau_vpn($db, $adh, $login, $prenom, $nom, $mail);
		}
		print "\n\n<p>--- end ok</p>\n\n";
		notifications($nomail, $login_vpn, $password);
	}
	else
	{
		print "\n\n<p>--- end error code=".$error."</p>\n\n";
		$db->rollback();
		if ($error_creation == 0) {
			// On a pu créer l'adhérent, mais on veut annuler cela
			$ldap = new Ldap();
			$result = $ldap->connect_bind();
			$ldap->delete("cn=$prenom $nom,ou=members,dc=aquilenet,dc=fr");
		}
	}

}

function nouvelle_souscription($db, $user, $file)
{
	$error = 0;
	$f = fopen($file,'r');
	if (!$f)
		$error++;
	fseek($f, 0, SEEK_END);
	$len = ftell($f);
	fseek($f, 0);
	$data = fread($f, $len);
	
	$valeurs = explode("\n", $data);
	
	$num=0;
	$personne = $valeurs[$num++];
	$prenom = $valeurs[$num++];
	$nom = $valeurs[$num++];
	$adr = $valeurs[$num++];
	$adrbis = $valeurs[$num++];
	$cp = $valeurs[$num++];
	$ville = $valeurs[$num++];
	$tel = $valeurs[$num++];
	$mail = $valeurs[$num++];
	$passwd = $valeurs[$num++];
	#$njour = $valeurs[$num++];
	#$nmois = $valeurs[$num++];
	#$nannee = $valeurs[$num++];
	$njour = 01;
	$nmois = 01;
	$nannee = 1970;
	$iban = $valeurs[$num++];
	$bic = $valeurs[$num++];
	$dat = $valeurs[$num++];
	$lieu = $valeurs[$num++];
	$adsltel = $valeurs[$num++];
	$tarif = $valeurs[$num++];
	$option = $valeurs[$num++];
	$rum = $valeurs[$num++];
	$vpn = $valeurs[$num++];
	$pgp = $valeurs[$num++];
	
	echo "\n\n<p>ajout de souscription pour $prenom $nom</p>\n\n";
	
	$jour=substr($dat,0,2);
	$mois=substr($dat,3,2);
	$annee=substr($dat,6);
	$date_fact = mktime(0, 0, 0, $mois, $jour, $annee);
	$date_nais = mktime(0, 0, 0, $nmois, $njour, $nannee);
	
	/* Virer les accents et majuscules */
	function sanitize($str) {
	  setlocale(LC_ALL, "fr_FR.UTF-8");
	  $str = iconv("UTF-8", "ASCII//TRANSLIT", $str);
	  $str = strtolower($str);
	  return $str;
	}
	
	$login = $prenom;
	if ($prenom && $nom)
	  $login .= ".";
	$login .= $nom;
	
	$login = sanitize($login);
	$tr =     "\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f".
	      "\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f".
	      "\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f".
	      "\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f".
	      "\x40".
	                                                  "\x5b\x5c\x5d\x5e\x5f".
	      "\x60".
	                                                  "\x7b\x7c\x7d\x7e\x7f";
	$dots = str_repeat('.', strlen($tr));
	$login = strtr($login, $tr, $dots);
	$login = preg_replace("/\.\.*/", ".", $login);
	
	$nom_complet = $prenom;
	if ($prenom && $nom)
	  $nom_complet .= " ";
	$nom_complet .= $nom;
	
	echo "\n\n<p>login $login</p>\n\n";
	
	// Start of transaction
	$db->begin();
	
	require_once(DOL_DOCUMENT_ROOT."/adherents/class/adherent.class.php");
	require_once(DOL_DOCUMENT_ROOT."/adherents/class/adherent_type.class.php");
	require_once(DOL_DOCUMENT_ROOT."/adherents/class/cotisation.class.php");
	require_once(DOL_DOCUMENT_ROOT."/user/class/user.class.php");
	require_once(DOL_DOCUMENT_ROOT."/compta/bank/class/account.class.php");
	require_once(DOL_DOCUMENT_ROOT."/societe/class/companybankaccount.class.php");
	require_once(DOL_DOCUMENT_ROOT."/product/class/product.class.php");
	require_once(DOL_DOCUMENT_ROOT."/compta/facture/class/facture-rec.class.php");
	require_once(DOL_DOCUMENT_ROOT."/aquilenet/aquilenet.lib.php");
	
	/*
	 * Récupérer l'adhérent
	 */
	
	$tiers = new Societe($db);
	$result = $tiers->fetch('', $nom_complet);
	if ($result <= 0) {
		print("\n\n<p>Impossible de récupérer le tiers</p>\n\n");
		$error++;
		dol_print_error($db,$tiers->error);
	}
	$adh = new Adherent($db);
	$result = $adh->fetch('', '', $tiers->id);
	if ($result <= 0) {
		print("\n\n<p>Impossible de récupérer l'adhérent(e)</p>\n\n");
		$error++;
		dol_print_error($db,$adh->error);
	}
	
	print("\n\n<p>$error erreurs pour l'instant</p>\n\n");
	
	/*
	 * Récupérer le rib pour l'adhérent(e)
	 */
	$account = new CompanyBankAccount($db);
	$result = $account->fetch(0,$tiers->id);
	if ($result <= 0) {
		print("\n\n<p>Impossible de récupérer les infos de RIB</p>\n\n");
		$error++;
		dol_print_error($db,$account->error());
	}
	
	print("\n\n<p>$error erreurs pour l'instant</p>\n\n");
	
	/* TODO: factoriser !! */
	if ($iban) {
		print("\n\n<p> ajout infos banque</p>\n\n");
		if ($account->bank && $account->iban != $iban) {
			print("\n\n\n<p>L'adhérent(e) a déjà des informations de prélèvement !</p>\n\n\n");
			$error++;
		}
		$account->number = "-";
		$account->iban_prefix = $iban;
		$account->iban = $iban;
		$account->bic = $bic;
		$account->proprio = $nom_complet;
		$account->adresse_proprio = "$adr\n$adrbis\n$cp $ville";
	
		$result = $account->update($user);
		if (!$result) {
			$error++;
			dol_print_error($db,$account->error());
		}
	}
	
	print("\n\n<p>$error erreurs pour l'instant</p>\n\n");
	
	/*
	 * Création de ligne ADSL
	 */
	
	print("\n\n<p>$error erreurs pour l'instant, maintenant ADSL</p>\n\n");
	
	if ($adsltel && $tarif && $option) {
		if (creer_adsl($db, $user, $adh, $nom_complet, $adsltel, $tarif, $option) < 0) {
			print("\n\n<p>erreur création ligne ADSL</p>\n");
			$error++;
		}
	}
	
	/*
	 * Création de connexion VPN
	 */
	
	print("\n\n<p>$error erreurs pour l'instant, maintenant VPN</p>\n\n");
	
	$password = "";
	$login_vpn = "";
	if ($vpn && $tarif) {
		$password = ranpass(8);
		$login_vpn = creer_vpn($db, $user, $tiers, $adh, $nom_complet, $password, $date_fact, $tarif);
		if (!$login_vpn) {
			print("\n\n<p>erreur création connexion VPN</p>\n");
			$error++;
		}
	}
	
	// décommenter pour tester sans commiter dans dolibarr
	//$error++;
	
	// -------------------- END OF YOUR CODE --------------------
	
	if (! $error)
	{
		$db->commit();
		$nomail = 0;
		if ($mail == "")
		{
			$nomail = 1;
			$mail = "$login@aquilenet.fr";
		}
	
		if ($adsltel && $tarif && $option) {
			note_nouvel_adsl($prenom, $nom, $mail);
		}
		if ($vpn && $tarif) {
			note_nouveau_vpn($db, $adh, $login, $prenom, $nom, $mail);
		}
		print "\n\n<p>--- end ok</p>\n\n";
		notifications($nomail, $login_vpn, $password);
	}
	else
	{
		print "\n\n<p>--- end error code=".$error."</p>\n\n";
		$db->rollback();
	}
}

function montrer_bilans_adherents($db)
{
	print("\n<p>bilans non nuls:</p>\n\n");

	$sql = "SELECT ba.ref, ba.label";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
	$sql.= " WHERE ( ba.ref >= 41100000 and ba.ref <= 41109999 ) or ba.ref = 51120000";
	$sql.= $db->order("ba.label", "ASC");

	print("<table>\n");
	$result = $db->query($sql);
	if ($result) {
		$num = $db->num_rows($results);
		for ($i = 0; $i < $num; $i++) {
			$obj = $db->fetch_object($result);
			$account = new Account($db);
			if ($account->fetch('', $obj->ref) <= 0)
				continue;
			$solde = $account->solde(0);
			$comment = $account->comment;
			if ($solde != 0.0 || $comment != '') {
				if ($comment == '') {
					$comment = '?';
				}
				print("<tr><td><a href=".DOL_URL_ROOT."/compta/bank/bankentries_list.php?id=".$account->id.">".$account->label."</a></td><td>".money($solde)."</td><td><a href=".DOL_URL_ROOT."/compta/bank/card.php?action=edit&id=".$account->id.">".$comment."</a></td></tr>\n");
			}
		}
	}
	print("</table>\n");
}

function montrer_bilan_comptable($db)
{
	print("\n<p>dettes/créances</p>\n\n");

	$sql = "SELECT ba.ref, ba.label";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
	$sql.= " WHERE ( ba.ref >= 40100000 and ba.ref <= 40109999 ) or ( ba.ref >= 41100000 and ba.ref <= 41109999 )";
	$sql.= $db->order("ba.label", "ASC");

	print("<table>\n");

	$somme_creances_adherents = 0;
	$somme_dettes_adherents = 0;
	$somme_creances_fournisseurs = 0;
	$somme_dettes_fournisseurs = 0;
	$somme_creances_autres = 0;
	$somme_dettes_autres = 0;

	$result = $db->query($sql);
	if ($result) {
		$num = $db->num_rows($results);
		for ($i = 0; $i < $num; $i++) {
			$obj = $db->fetch_object($result);
			$account = new Account($db);
			if ($account->fetch('', $obj->ref) <= 0)
				continue;
			$solde = $account->solde(0);
			$comment = $account->comment;
			if ($solde != 0.0) {
				if ($obj->ref >= 40100000 and $obj->ref <= 40109999) {
					# fournisseur
					if ($solde > 0)
						$somme_dettes_fournisseurs = $somme_dettes_fournisseurs + $solde;
					else
						$somme_creances_fournisseurs = $somme_creances_fournisseurs + $solde;
				} else if ($obj->ref >= 41100000 and $obj->ref <= 41109999) {
					# adhérent
					if ($solde > 0)
						$somme_dettes_adherents = $somme_dettes_adherents + $solde;
					else
						$somme_creances_adherents = $somme_creances_adherents + $solde;
				}
				print("<tr><td><a href=".DOL_URL_ROOT."/compta/bank/bankentries_list.php?id=".$account->id.">".$account->label."</a></td><td>".money($solde)."</td><td><a href=".DOL_URL_ROOT."/compta/bank/card.php?action=edit&id=".$account->id.">".$comment."</a></td></tr>\n");
			}
		}
	}
	print("</table>\n");
	print("<p>Total créances adhérents: $somme_creances_adherents</p>");
	print("<p>Total créances fournisseurs: $somme_creances_fournisseurs</p>");
	print("<p>Total dettes adhérents: $somme_dettes_adherents</p>");
	print("<p>Total dettes fournisseurs: $somme_dettes_fournisseurs</p>");
}

function do_prelevements($db, $user, $file, $test)
{
	global $dolibarr_prenom;
	global $dolibarr_nom;
	global $COMPTE_COMPTE_COURANT;
	global $COMPTE_PRELEVEMENTS;

	// Start of transaction
	$db->begin();

	require_once(DOL_DOCUMENT_ROOT."/aquilenet/aquilenet.lib.php");
	require_once(DOL_DOCUMENT_ROOT."/adherents/class/adherent.class.php");

	$error = 0;
	$f = fopen($file,'r');
	if (!$f)
		$error++;

	// On commence à créer de nouveaux prélèvements
	$now = strftime("%Y-%m-%d %H:%M:%S", dol_now());

	print("<table>");
	print("<tr><th>Réf compte</th><th>Prénom</th><th>Nom</th><th>Réf adh</th><th>date</th><th>montant</th><th>solde</th></tr>");
	$total = 0;
	$last_date = 0;
	while(true) {
		// Récupérer une ligne
		$s = fgets($f);
		if (!$s)
			break;
		//print("*** $s");

		// Décomposer
		$valeurs = explode(";", $s);

		$num = intval(str_replace('=','',str_replace('"','',$valeurs[6])));
		$etat = $valeurs[10];
		$nom = str_replace('=','',str_replace('"','',$valeurs[27]));
		$prenom = str_replace('=','',str_replace('"','',$valeurs[28]));
		$date = $valeurs[9];
		$date_array = strptime($date, "%d/%m/%y");
		$amount = floatval(str_replace(",", ".", $valeurs[8]));

		if ($valeurs[1] == "Num\xe9ro du compte \xe9metteur" || $valeurs[1] == "Numéro du compte émetteur")
			continue;

		if ($etat != "En attente d'ex\xe9cution" && $etat != "En attente d'exécution" &&
		    $etat != "Ex\xe9cut\xe9e" && $etat != "Exécutée" &&
		    $etat != "Impay\xe9e" && $etat != "Impayée" &&
		    $etat != "En cours d'ex\xe9cution" && $etat != "En cours d'exécution")
		{
			print("<tr><td></td><td></td><td>pas ".$prenom."</td><td>".$nom."</td><td>'".$etat."'\n</td></tr>");
			continue;
		}

		// Analyser
		$the_date = mktime(0,0,0,$date_array["tm_mon"]+1,$date_array["tm_mday"],$date_array["tm_year"]+1900);

		//print("\n\ngot $num $date $amount\n\n");

		// Pour le mois prochain
		if ($the_date > time() + (30+15)*24*3600)
			continue;

		if ($the_date > $last_date)
			$last_date = $the_date;

		$total += $amount;

		$adh = new Adherent($db);
		$adh->fetch($num);

		$account = compte_adherent($db, $adh);

		$res = get_transfer($db, $COMPTE_PRELEVEMENTS, $account->id, $the_date, $user);
		if (transfer($db, $COMPTE_PRELEVEMENTS, $account->id, $the_date, "Prélèvement ".$adh->$dolibarr_prenom." ".$adh->$dolibarr_nom, $amount, $user)) {
			print("\n\n erreur transfert prélèvement\n\n");
			$error++;
		}
		if ($res[0] != -1 and $res[1] < $now) {
			print("<p><b><font color='red'>Il y a déjà un prélèvement pour ".$adh->$dolibarr_prenom." ".$adh->$dolibarr_nom." à la date $date !! (créé le ".$res[1].")</font></b></p>\n");
			$amount = "<b><font color='red'>".$res[0]." vs $amount</font></b>";
			$error++;
		}

		// Debug
		print("<tr><td>".$account->id. "</td> <td>" .$adh->$dolibarr_prenom."</td> <td>".$adh->$dolibarr_nom."</td> <td>$num</td> <td>".strftime("%d/%m/%Y", $the_date)."</td> <td>$amount</td> <td>". $account->solde(0). "</td>\n</tr>");


	}

	if ($test == 0) {
		if (transfer($db, $COMPTE_COMPTE_COURANT, $COMPTE_PRELEVEMENTS, $last_date, "Prélèvements adhérents ".strftime("%m/%Y", $last_date - (15*24*3600)), $total, $user)) {
			print("\n\n erreur transfert prélèvement\n\n");
			$error++;
		}
	}

	print(strftime("<tr><td>Total")." $total</td></tr>\n");
	print("</table>");

	montrer_bilans_adherents($db);

	if ($test != 0) {
		print("<p>on provoque une erreur exprès pour que ce soit juste une simulation</p>\n");
		$error++;
	}

	// -------------------- END OF YOUR CODE --------------------

	if (! $error)
	{
		$db->commit();
		print '--- end ok'."\n";
	}
	else
	{
		print '--- end error code='.$error."\n";
		$db->rollback();
	}
}

function test_prelevements($db, $user, $file)
{
	do_prelevements($db, $user, $file, 1);
}

function import_prelevements($db, $user, $file)
{
	do_prelevements($db, $user, $file, 0);
}

function rappel_prelevements($db, $user)
{
	global $dolibarr_prenom;
	global $dolibarr_nom;
	global $COMPTE_PRELEVEMENTS;

	// On montre les écritures à partir du début du mois précédent
	$prev = dol_get_prev_month(date("m"),date("Y"));
	$start_mois = dol_mktime(0,0,0,$prev['month'],1,$prev['year']);
	
	// On regarde les prélèvements prévus à partir du 25 de ce mois-ci
	$start_prelevement = dol_mktime(0,0,0,date("m"),25,date("Y"));
	
	$sql = "SELECT b.rowid, b.datev as dv, b.label, b.amount";
	$sql.= ", ba2.ref";
	$sql.= " FROM ";
	$sql.= "  ".MAIN_DB_PREFIX."bank_account as ba2";
	$sql.= ", ".MAIN_DB_PREFIX."bank as b";
	$sql.= ", ".MAIN_DB_PREFIX."bank as b2";
	$sql.= ", ".MAIN_DB_PREFIX."bank_url as bu";
	$sql.= " WHERE b.fk_account=".$COMPTE_PRELEVEMENTS;
	$sql.= " AND bu.fk_bank = b.rowid";
	$sql.= " AND bu.url_id = b2.rowid";
	$sql.= " AND b2.fk_account = ba2.rowid";
	$sql.= " AND b.datev >= '".$db->idate($start_prelevement)."'";
	$sql.= $db->order("b.datev, b.datec", "ASC");
	
	$result = $db->query($sql);

	if ($result) {
		$total = 0;
		$num = $db->num_rows($result);
		print "$num prélèvements <br/>\n";
		for ($i = 0; $i < $num; $i++) {
			$obj = $db->fetch_object($result);
			$ref = $obj->ref;
			$amount = $obj->amount;
			$amountstr = number_format(-$amount, 2, ",", " ");
			if ($ref == 51200001)
			{
				//print("prélèvement\n");
			}
			else if ((int) ($ref / 1000) == 41100)
			{
				$refadh = $ref % 1000;
				// Adhérent
				$adh = new Adherent($db);
				$adh->fetch('', $refadh);
				$prenom = $adh->$dolibarr_prenom;
				print("adhérent⋅e $refadh: ".$adh->$dolibarr_prenom." ".$adh->$dolibarr_nom." ".$adh->email." ".$amountstr."€ <br/>\n");
	
				// Récupérer les dernières écritures
				$ecritures = "";
				$sql = "SELECT b.rowid, b.datev as dv, b.label, b.amount";
				$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
				$sql.= ", ".MAIN_DB_PREFIX."bank as b";
				$sql.= " WHERE ba.ref=".$ref;
				$sql.= " AND b.fk_account = ba.rowid";
				$sql.= " AND b.datev >= '".$db->idate($start_mois)."'";
				$sql.= " AND b.amount < 0";
				$sql.= $db->order("b.datev, b.datec", "ASC");
				$resultadh = $db->query($sql);
				if ($resultadh) {
					$totaladh = 0;
					$numadh = $db->num_rows($resultadh);
					for ($j = 0; $j < $numadh; $j++) {
						$objadh = $db->fetch_object($resultadh);
						$ecritures .= dol_print_date($db->jdate($objadh->dv))." ".$objadh->label." ".number_format($objadh->amount, 2, ",", " ")."€\n";
					}
				}
				//print($ecritures);
	
	
				//if ($refadh == 100) // samuel
				//if ($refadh == 111) // pablo
				mymailadh($db, $adh, "Prochain prelevement Aquilenet" ,
	"Bonjour $prenom,

Pour information, le montant de votre prochain prélèvement est de

$amountstr €

et voici les créances des deux mois passés vous concernant,
expliquant probablement ce montant de prélèvement :

$ecritures
L'ensemble de toutes les écritures comptables vous concernant est
comme d'habitude disponible sur votre espace adhérent:

https://adherents.aquilenet.fr/compta.php

Ce mail est envoyé lors de l'établissement de la comptabilité pour
que vous puissiez éventuellement y réagir pour que l'on corrige,
et le 28 du mois pour rappel.

Associativement,
Les trésorières et trésoriers, pour la collégiale d'Aquilenet");
			}
			else
			{
				printf("numéro de compte bizarre $ref\n");
			}
		}
	}
}

function compte_produits($db, $pid)
{
	$qty = 0;
	$sql = "SELECT cd.qty";
	$sql .= " FROM ".MAIN_DB_PREFIX."contrat as c,";
	$sql .= " ".MAIN_DB_PREFIX."contratdet as cd";
	$sql .= " LEFT JOIN ".MAIN_DB_PREFIX."product as p ON cd.fk_product = p.rowid";
	$sql .= " WHERE";
	$sql .= " c.rowid = cd.fk_contrat";
	$sql .= " AND p.rowid = ".$pid;
	$sql .= " AND cd.statut = 4";
	$resql = $db->query($sql);
	if ($resql)
	{
		$numr = $db->num_rows($resql);
		$j = 0;
		while ($j < $numr)
		{
			$objp = $db->fetch_object($resql);
			$qty += $objp->qty;
			$price += $objp->qty * $objp->prix;
			$j++;
		}
	}
	if (!$qty)
		return 0;
	else
		return $qty;
}

function compte_vpn($db)
{
	return compte_produits($db,20) + compte_produits($db,21);
}

function compte_adsl($db)
{
	$sql = "SELECT label FROM ".MAIN_DB_PREFIX."bank_account";
	$sql .= " WHERE clos = 0";
	$sql .= " AND label LIKE 'Ligne %'";
	$resql = $db->query($sql);
	return $db->num_rows($resql);
}

function compte_adherents($db)
{
	$sql = "SELECT datefin FROM ".MAIN_DB_PREFIX."adherent";
	$now = dol_now();
	$sql .= " WHERE datefin >= '".$db->idate($now-11*31*24*3600)."'";
	$resql = $db->query($sql);
	return $db->num_rows($resql);
}

function print_compte_produits($db, $fk_soc, $pid, $print_price)
{
	$qty = 0;
	$price = 0;
	$sql = "SELECT cd.qty, cd.subprice as prix";
	$sql .= " FROM ".MAIN_DB_PREFIX."contrat as c,";
	$sql .= " ".MAIN_DB_PREFIX."contratdet as cd";
	$sql .= " LEFT JOIN ".MAIN_DB_PREFIX."product as p ON cd.fk_product = p.rowid";
	$sql .= " WHERE";
	$sql .= " c.rowid = cd.fk_contrat";
	$sql .= " AND c.fk_soc = ".$fk_soc;
	$sql .= " AND p.rowid = ".$pid;
	$sql .= " AND cd.statut = 4";
	$resql = $db->query($sql);
	if ($resql)
	{
		$numr = $db->num_rows($resql);
		$j = 0;
		while ($j < $numr)
		{
			$objp = $db->fetch_object($resql);
			$qty += $objp->qty;
			$price += $objp->qty * $objp->prix;
			$j++;
		}
	}
	if (!$qty)
		$qty = "";
	else
		$qty = number_format($qty, 2, ",", "");
	if (!$price)
		$price = "";
	else
		$price = number_format($price, 2, ",", "");
	if ($print_price)
		echo $qty.";".$price.";";
	else
		echo $qty.";";
}

function export_sous($db) {
	global $dolibarr_prenom;
	global $dolibarr_nom;
	global $dolibarr_ville;
	global $dolibarr_note;
	header('Content-type: application/octet-stream');
	header('Content-Disposition: attachment; filename=sous.csv');

	$sql = "SELECT rowid, $dolibarr_prenom";
	$sql.= " FROM ".MAIN_DB_PREFIX."adherent";
	$sql.= " ORDER by $dolibarr_prenom,$dolibarr_nom ASC";

	$resql = $db->query($sql);
	if ($resql)
	{
		$numr = $db->num_rows($resql);
		$i = 0;
		while ($i < $numr)
		{
			$objp = $db->fetch_object($resql);
			$adherent = new Adherent($db);
			if ($adherent->fetch('', $objp->rowid) >= 1) {
				echo $adherent->$dolibarr_prenom." ".$adherent->$dolibarr_nom.";";
				echo $adherent->$dolibarr_ville.";";
				$statut = $adherent->statut;
				$type = $adherent->typeid;
				$actif = 0;
				$preferentiel = 0;
				$bienfaiteur = 0;
				if ($statut != 0)
				{
					if ($type == 1)
						$actif++;
					else if ($type == 2)
						$preferentiel++;
					else if ($type == 3)
						$bienfaiteur++;
				}
				if (!$actif)
					$actif = "";
				if (!$preferentiel)
					$preferentiel = "";
				if (!$bienfaiteur)
					$bienfaiteur = "";
				echo $actif.";".$preferentiel.";".$bienfaiteur.";";
				echo ";";
				$dg5 = 0;
				$dg = 0;
				$ndg = 0;
				$dg5p = 0;
				$dgp = 0;
				$ndgp = 0;
				$l = 0;
				$lp = 0;
				$ldt = 0;
				$ldtp = 0;
				if ($adherent->$dolibarr_note)
				{
					$lignes = explode("\n", $adherent->$dolibarr_note);
					// préférentiel
					foreach ($lignes as $ligne) {
						$ligne = trim($ligne);
						$account = new Account($db);
						if ($account->fetch('', $ligne) >= 1 && substr($account->label,0,5) == "Ligne") {
							$infos = explode("\n", $account->comment);
							$debit = trim($infos[1]);
							$tarif = trim($infos[2]);
							$mensualite = trim($infos[3]);
							if ($mensualite == 25)
								$dg5++;
							else if ($mensualite == 23.5)
								$dg5p++;
							else if (substr($debit,-4)=="opt1")
							{
								if ($tarif == "preferentiel")
									$dgp++;
								else
									$dg++;
							}
							else if (substr($debit,-4)=="opt3" || substr($debit,-4)=="opt5")
							{
								if ($tarif == "preferentiel")
									$ndgp++;
								else
									$ndg++;
							}
							else if (substr($debit,-13)=="LiazoOrangeNu")
							{
								if ($tarif == "preferentiel")
									$ldtp++;
								else
									$ldt++;
							}
							else if (substr($debit,-11)=="LiazoOrange")
							{
								if ($tarif == "preferentiel")
									$lp++;
								else
									$l++;
							}
							else
							{
								print("Ooops, option inconnue $debit");
							}
						}
					}
				}
				if (!$dg5) $dg5 = "";
				if (!$dg) $dg = "";
				if (!$ndg) $ndg = "";
				if (!$dg5p) $dg5p = "";
				if (!$dgp) $dgp = "";
				if (!$ndgp) $ndgp = "";
				if (!$l) $l = "";
				if (!$lp) $lp = "";
				if (!$ldt) $ldt = "";
				if (!$ldtp) $ldtp = "";
				echo $dg5.";".$dg.";".$ndg.";;".$dg5p.";".$dgp.";".$ndgp.";";
				echo ";";
				echo $l.";".$ldt.";".$lp.";".$ldtp.";";
				echo ";";

				// owncloud
				print_compte_produits($db, $adherent->fk_soc, 18, 1);
				// NFS
				print_compte_produits($db, $adherent->fk_soc, 17, 1);
				// local
				print_compte_produits($db, $adherent->fk_soc, 19, 1);
				echo ";";

				// VPN
				print_compte_produits($db, $adherent->fk_soc, 20, 0);
				// VPN-P
				print_compte_produits($db, $adherent->fk_soc, 21, 0);
				// IPv4 fixe
				print_compte_produits($db, $adherent->fk_soc, 32, 0);
				// Wifi
				print_compte_produits($db, $adherent->fk_soc, 33, 1);
				echo ";";

				// VM
				print_compte_produits($db, $adherent->fk_soc, 31, 1);
				// Mezzanine
				print_compte_produits($db, $adherent->fk_soc, 30, 1);
				// Cogent
				print_compte_produits($db, $adherent->fk_soc, 35, 1);
				// BGP Cogent
				print_compte_produits($db, $adherent->fk_soc, 36, 1);
				// Transit
				print_compte_produits($db, $adherent->fk_soc, 38, 1);
			}
			echo "\n";
			$i++;
		}
	}
}


function get_cotis($db, $id) {
	global $dolibarr_cotisation;
	$sql = "SELECT c.$dolibarr_cotisation";
	$sql.= " FROM ".MAIN_DB_PREFIX."$dolibarr_cotisation as c";
	$sql.= " WHERE c.fk_adherent = ".$id;
	$sql.= " ORDER by c.datef DESC LIMIT 1";
	$resql = $db->query($sql);
	if ($resql)
	{
		$val = $db->fetch_object($resql)->$dolibarr_cotisation;
		return $val;
	}
	return -1;
}


?>
